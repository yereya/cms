<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/****************************************
 * CMS Sites
 ****************************************/

$factory->define(App\Entities\Models\Sites\Site::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'is_public' => true,
        'db_host' => 'localhost',
        'db_user' => 'root',
        'db_pass' => '',
        'db_name' => 'top5_' . $faker->word
    ];
});

$factory->define(App\Entities\Models\Sites\Domain::class, function (Faker\Generator $faker) {
    return [
        'site_id' => 1,
        'domain' => $faker->domainName
    ];
});

/****************************************
 * CMS Widgets
 ****************************************/

$factory->define(App\Entities\Models\Widgets\Widget::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'controller' => '/some/path/to/controller'
    ];
});

$factory->define(App\Entities\Models\Widgets\FieldType::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'controller' => '/some/path/to/controller'
    ];
});

$factory->define(App\Entities\Models\Widgets\Field::class, function (Faker\Generator $faker) {
    return [
        'widget_id' => null,
        'widget_field_type_id' => null,
        'title' => $faker->name,
        'default' => null,
        'help' => $faker->text
    ];
});

/****************************************
 * CMS Users
 ****************************************/

$factory->define(App\Entities\Models\Users\User::class, function (Faker\Generator $faker) {
    return [
        'email' => $faker->email,
        'username' => $faker->userName,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'password' => $faker->randomDigitNotNull
    ];
});

$factory->define(App\Entities\Models\Users\AccessControl\Role::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->text
    ];
});

$factory->define(App\Entities\Models\Users\AccessControl\Permission::class, function (Faker\Generator $faker) {
    return [
        'type' => $faker->shuffleArray(['view', 'add', 'edit', 'delete'])[0],
        'name' => $faker->word,
        'description' => $faker->text
    ];
});

$factory->define(App\Entities\Models\Users\Notifications\Rule::class, function (Faker\Generator $faker) {
    return [
        'type' => $faker->shuffleArray(['error', 'warning', 'information', 'announcement'])[0],
        'message' => $faker->text
    ];
});

$factory->define(App\Entities\Models\Users\Notifications\Notification::class, function (Faker\Generator $faker) {
    return [
        'user_id' => null,
        'user_notification_rule_id' => null,
        'location' => $faker->shuffleArray(['top', 'modal'])[0],
        'is_viewed' => false
    ];
});