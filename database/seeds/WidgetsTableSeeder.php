<?php

use App\Entities\Models\Widgets\Field as CMS_Field;
use App\Entities\Models\Widgets\FieldType as CMS_FieldType;
use App\Entities\Models\Widgets\Widget as CMS_Widget;
use Illuminate\Database\Seeder;

class WidgetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $widgets     = factory(CMS_Widget::class, 2)->create();
        $field_types = factory(CMS_FieldType::class, 5)->create();

        $widgets->each(function ($widget) use ($field_types) {
            $field_types->each(function ($field_type) use ($widget) {
                factory(CMS_Field::class)->create([
                    'widget_id' => $widget->id,
                    'widget_field_type_id' => $field_type->id
                ]);
            });
        });
    }
}
