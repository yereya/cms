<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangesInReportQuerySelectors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_query_selectors', function (Blueprint $table) {
            $table->renameColumn('location_name', 'clause_type');
            $table->renameColumn('exists_in_base_query', 'order');
            $table->renameColumn('selector', 'field_name');
            $table->dropColumn('deleted_at');
        });

        Schema::rename('reports_query_selectors', 'reports_query_fields');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('reports_query_fields', 'reports_query_selectors');

        Schema::table('reports_publishers_keywords', function (Blueprint $table) {
            $table->renameColumn('filter_type', 'location_name');
            $table->renameColumn('selector_default_order', 'exists_in_base_query');
            $table->removeColumn('field_name', 'selector');
            $table->softDeletes();
        });
    }
}
