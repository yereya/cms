<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesPageSpeedScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites_page_speed_scores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('site_url', 512);
            $table->integer('desktop_score')->unsigned();
            $table->integer('mobile_score')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sites_page_speed_scores');
    }
}
