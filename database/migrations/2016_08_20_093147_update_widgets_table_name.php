<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWidgetsTableName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('widgets', 'site_widgets');
        Schema::rename('widget_fields', 'site_widget_fields');
        Schema::rename('widget_field_types', 'site_widget_field_types');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('site_widgets', 'widgets');
        Schema::rename('site_widget_fields', 'widget_fields');
        Schema::rename('site_widget_field_types', 'widget_field_types');
    }
}
