<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToReportsQuerySelectors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_query_selectors', function (Blueprint $table) {
            $table->string('display_name')->nullable();
            $table->string('exists_in_base_query')->nullable();
            $table->string('location_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_query_selectors', function (Blueprint $table) {
            $table->dropColumn('display_name');
            $table->dropColumn('exists_in_base_query');
            $table->dropColumn('location_name');
        });
    }
}
