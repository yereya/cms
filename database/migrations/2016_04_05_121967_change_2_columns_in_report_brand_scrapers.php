<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Change2ColumnsInReportBrandScrapers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $platform = Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform();
        $platform->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('reports_brand_scrapers', function (Blueprint $table) {
            $table->renameColumn('run_time_end', 'run_time_stop');
            $table->renameColumn('run_minutes_interval', 'repeat_delay');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_brand_scrapers', function (Blueprint $table) {
            $table->renameColumn('run_time_stop', 'run_time_end');
            $table->renameColumn('repeat_delay', 'run_minutes_interval');
        });
    }
}
