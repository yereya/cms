<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyNameAndCompanyIdFieldsIntoOutBrands extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('bo')
            ->table('out_brands', function (Blueprint $table) {
                $table->integer('company_id');
                $table->string('company_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('bo')
            ->table('out_brands', function (Blueprint $table) {
            $table->dropColumn(['company_id','company_name']);
        });
    }
}
