<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdveriserIdReportsPublishersAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_publishers_accounts', function (Blueprint $table) {
            $table->string('advertiser_id', 100)->nullable();
            $table->foreign('advertiser_id')->references('id')->on('reports_publishers_advertisers')
                ->onDelete('no action')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_publishers_accounts', function (Blueprint $table) {
            $table->dropColumn('advertiser_id');
            $table->dropForeign('reports_publishers_accounts_advertiser_id_foreign');
        });
    }
}
