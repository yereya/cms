<?php
Route::group([], function () {
    Route::get('logs', 'TpLoggerController@index')
        ->name('logs.index');
    Route::get('logs/{log_id}/list-group', 'TpLoggerController@listGroup')
        ->name('logs.{log_id}.list-group');
    Route::get('logs/{log_id}/show-modal', 'TpLoggerController@showModal')
        ->name('logs.{log_id}.show-modal');
    Route::get('logs/{log_id}/show-dwh-modal', 'TpLoggerController@showDwhLogModal')
        ->name('logs.{log_id}.show-dwh-modal');
    Route::post('logs/datatable', 'TpLoggerController@datatable')
        ->name('logs.datatable');
    Route::get('logs/select2', 'TpLoggerController@select2')
        ->name('logs.select2');
});