<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Analytics account id
    |--------------------------------------------------------------------------
    |
    | Source account id values for google accounts
    */

    'account_id' => '60524656',

    /*
    |--------------------------------------------------------------------------
    | Tracking Ids
    |--------------------------------------------------------------------------
    |
    | Source account id values for google accounts
    |
    | Format: {adwords account_id} => {analytics tracking_id(tid)}
    |
    */

    'tids' => [
        'http://top5casinosites.co.uk/' => 'UA-60524656-2'
    ],

    /*
    |--------------------------------------------------------------------------
    | SMTP Host Address
    |--------------------------------------------------------------------------
    |
    | Here you may provide the host address of the SMTP server used by your
    | applications. A default option is provided that is compatible with
    | the Mailgun mail service which will provide reliable deliveries.
    |
    */

    'cids' => ['YOUR', 'CLIENT', 'IDS'],

];
