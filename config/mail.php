<?php

return [

    'stream' => [
        'ssl' => [
            'allow_self_signed' => true,
            'verify_peer' => false,
            'verify_peer_name' => false,
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Mail Driver
    |--------------------------------------------------------------------------
    |
    | Laravel supports both SMTP and PHP's "mail" function as drivers for the
    | sending of e-mail. You may specify which one you're using throughout
    | your application here. By default, Laravel is setup for SMTP mail.
    |
    | Supported: "smtp", "mail", "sendmail", "mailgun", "mandrill", "ses", "log"
    |
    */

    'driver' => env('MAIL_DRIVER', 'smtp'),

    /*
    |--------------------------------------------------------------------------
    | SMTP Host Address
    |--------------------------------------------------------------------------
    |
    | Here you may provide the host address of the SMTP server used by your
    | applications. A default option is provided that is compatible with
    | the Mailgun mail service which will provide reliable deliveries.
    |
    */

    'host' => env('MAIL_HOST', 'smtp.mailgun.org'),

    /*
    |--------------------------------------------------------------------------
    | SMTP Host Port
    |--------------------------------------------------------------------------
    |
    | This is the SMTP port used by your application to deliver e-mails to
    | users of the application. Like the host we have set this value to
    | stay compatible with the Mailgun e-mail application by default.
    |
    */

    'port' => env('MAIL_PORT', 587),

    /*
    |--------------------------------------------------------------------------
    | Global "From" Address
    |--------------------------------------------------------------------------
    |
    | You may wish for all e-mails sent by your application to be sent from
    | the same address. Here, you may specify a name and address that is
    | used globally for all e-mails that are sent by your application.
    |
    */

    'from' => ['address' => 'cms@trafficpointltd.com', 'name' => 'Traffic Point CMS'],

    /*
    |--------------------------------------------------------------------------
    | E-Mail Encryption Protocol
    |--------------------------------------------------------------------------
    |
    | Here you may specify the encryption protocol that should be used when
    | the application send e-mail messages. A sensible default using the
    | transport layer security protocol should provide great security.
    |
    */

    'encryption' => env('MAIL_ENCRYPTION', 'tls'),

    /*
    |--------------------------------------------------------------------------
    | SMTP Server Username
    |--------------------------------------------------------------------------
    |
    | If your SMTP server requires a username for authentication, you should
    | set it here. This will get used to authenticate with your server on
    | connection. You may also set the "password" value below this one.
    |
    */

    'username' => env('MAIL_USERNAME'),

    /*
    |--------------------------------------------------------------------------
    | SMTP Server Password
    |--------------------------------------------------------------------------
    |
    | Here you may set the password required by your SMTP server to send out
    | messages from your application. This will be given to the server on
    | connection so that the application will be able to send messages.
    |
    */

    'password' => env('MAIL_PASSWORD'),

    /*
    |--------------------------------------------------------------------------
    | Sendmail System Path
    |--------------------------------------------------------------------------
    |
    | When using the "sendmail" driver to send e-mails, we will need to know
    | the path to where Sendmail lives on this server. A default path has
    | been provided here, which will work well on most of your systems.
    |
    */

    'sendmail' => '/usr/sbin/sendmail -bs',

    /*
    |--------------------------------------------------------------------------
    | Mail "Pretend"
    |--------------------------------------------------------------------------
    |
    | When this option is enabled, e-mail will not actually be sent over the
    | web and will instead be written to your application's logs files so
    | you may inspect the message. This is great for local development.
    |
    */

    'pretend' => env('MAIL_PRETEND', false),

    /*
    |--------------------------------------------------------------------------
    | Affiliates Mail
    |--------------------------------------------------------------------------
    |
    | Mail username and password for the affiliates account reports are being sent to
    |
    */
    'affiliates' => [
        'username' => env('MAIL_AFFILIATES_USERNAME'),
        'password' => env('MAIL_AFFILIATES_PASSWORD'),
        'valid_extensions' => ['xls', 'xlx', 'xlsx', 'csv', 'txt', 'html', 'xml'],
        'imap_gmail_connection' => '{imap.gmail.com:993/imap/ssl}'
    ],

    'page_speed_mail_alerts' => [
        'username' => 'ita@trafficpoint.io', 'nivi@trafficpoint.io', 'uzia@trafficpoint.io','guy.moyal@trafficpoint.io',
        'sites' => [
        
            'https://playright.co.uk/',

            'https://playright.co.uk/casino/page/slots',

            'https://playright.co.uk/casino/page/main',

            'https://playright.co.uk/betting/page/sports',
			
			'https://playright.co.uk/bingo/page/main-page/',
			
			'https://top5voipproviders.com/',

            'https://top5projectmanagement.com',

            'https://top5vpn.de/',

            'https://top5-crm.com/',
			
			'https://top5-medicalalertsystems.com/',
			
			'https://top5projectmanagement.com/',
			
			'https://top-posproviders.com/',
			
			'https://top5-mattresses.com/best-mattresses/',
			
			'https://topmattresses.co.uk/',
			
			'https://lendstart.com/personal-loans/main/',
			
			'https://top5-datingsites.com/',
			
			'https://top5-datingsites.co.uk/',
			
			'https://top-posproviders.com/',
			
			'https://top5-websitebuilders.com/'
        ]
    ]

];
