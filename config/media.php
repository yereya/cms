<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Media directory
    |--------------------------------------------------------------------------
    |
    | Directory where media is going to be saved
    | relatively to public
    |
    */

    'directory' => '/top-assets/sites/',

    /*
    |--------------------------------------------------------------------------
    | Allowed media types
    |--------------------------------------------------------------------------
    |
    | Comma separated short mime types of media
    | file types which can be uploaded
    |
    */

    'media_types' => 'jpeg,jpg,png,gif,svg,avi,mp4',

    /*
    |--------------------------------------------------------------------------
    | Upload Max File Size
    |--------------------------------------------------------------------------
    |
    | Maximum file size in KB
    |
    */

    'max_size' => 2048,

    /*
    |--------------------------------------------------------------------------
    | imageOptim auth
    |
    | To sign in https://imageoptim.com/api
    |--------------------------------------------------------------------------
    |
    */

    'image_optim_username' => 'bbrsssxksg'
];
