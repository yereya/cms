<?php namespace App\Libraries\Scraper\Requests\SearchQueryPerformance\Providers\Google\Translators;

/**
 * Class SearchQueryConversionPerformanceReport
 *
 * @package App\Libraries\Scraper\Requests\SearchQueryPerformance\Providers\Google\Translators
 */
class SearchQueryConversionPerformanceReport extends SearchQueryPerformanceReport
{
    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_search_query_conversion_performance';

    /**
     * @var int $report_id
     */
    protected $report_id = 17;

    /**
     * Build Rows from stream
     *
     * @param $stream
     *
     * @return array
     */
    protected function buildRows($stream)
    {
        $rows = [];
        foreach ($stream as $row) {
            if (is_null($row)) {
                continue;
            }
            if (property_exists($stream, 'date')) {
                $row['_ReportDate'] = $stream->date;
            }
            //in this report conversion value in format 1,097.45 
            $row['ConversionValue'] = str_replace(',', '', $row['AllConversionValue']);

            $rows[] = $this->getRowValues($row);
        }

        return $rows;
    }
}