<?php namespace App\Libraries\Scraper\Requests\Accounts\Providers\Bing\Translators;

use App\Entities\Models\Bo\Account;
use App\Libraries\Scraper\Translator;

/**
 * Class BIAccounts
 *
 * @package App\Libraries\Scraper\Requests\Accounts\Providers\Bing\Translators;
 */
class BIAccounts extends Translator
{
    /**
     * @var string $model_name
     */
    protected $model_name = Account::class;

    /**
     * @var string $connection
     */
    protected $connection = 'bo';

    /**
     * @var string $table_name
     */
    protected $table_name = 'accounts';

    /**
     * @var int $report_id
     */
    protected $report_id = 5;

    /**
     * @var int provider id
     */
    protected $provider_id = 97;

    /**
     * @var string $provider
     */
    protected $provider = 'Bing';

    /**
     * @var string $account_id_column
     */
    protected $account_id_column = 'source_account_id';

    /**
     * Build Rows from stream
     *
     * @param $stream
     *
     * @return array
     */
    protected function buildRows($stream)
    {
        $local_accounts = $this->getLocalAccounts();
        $rows = [];
        foreach ($stream as $row) {
            if (!$local_accounts->get($row->Id)) {
                $rows[] = $this->getRowValues($row);
            }
        }

        return $rows;
    }

    /**
     * Get Local Accounts
     *
     * @return mixed
     */
    private function getLocalAccounts() {
        return Account::where('publisher_id', $this->provider_id)->get()->keyBy('source_account_id');
    }
}