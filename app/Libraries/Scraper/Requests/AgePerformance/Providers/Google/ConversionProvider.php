<?php namespace App\Libraries\Scraper\Requests\AgePerformance\Providers\Google;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class ConversionProvider extends Provider
{
    /**
     * @var int $report_id
     */
    protected $report_id = 33;

    /**
     * @var string
     */
    protected $report_folder_name = 'AgeConversionsPerformance';
}