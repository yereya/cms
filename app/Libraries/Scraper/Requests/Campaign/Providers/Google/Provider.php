<?php namespace App\Libraries\Scraper\Requests\Campaign\Providers\Google;

use Exception;
use App\Entities\Models\Bo\Account;
use Google\AdsApi\AdWords\AdWordsSession;
use Google\AdsApi\AdWords\AdWordsServices;
use App\Entities\Models\Bo\AccountCampaign;
use App\Libraries\Scraper\Providers\GoogleReport;
use App\Libraries\Scraper\Requests\Campaign\Providers\SegmentTrait;
use Google\AdsApi\AdWords\v201809\cm\CampaignService;
use Google\AdsApi\AdWords\v201809\cm\Selector;

class Provider extends GoogleReport
{
    use SegmentTrait;

    /**
     * @var string
     */
    protected $report_type = 'CAMPAIGNS_REPORT';

    /**
     * @var int
     */
    protected $report_id = 14;

    /**
     * @Override parent method
     * start this script
     */
    public function fetch()
    {
        $this->logger->info('Start fetch');

        $customers = $this->getCustomers();

        $this->importCampaigns($customers);
    }

    /**
     * Get Customers By Advertisers, only mcc 1
     */
    private function getCustomers()
    {
        return Account::where('publisher_id', $this->publisher_id)
                        ->where('mcc', 1)
                        ->where('status', 'Active')
                        ->with('campaigns')
                        ->get();
    }

    /**
     * Import Campaigns
     * Store Campaigns or Update
     *
     * @param $customers - local accounts
     */
    private function importCampaigns($customers)
    {
        foreach ($customers as $account) {
            $campaigns = $this->execute($account);
            $this->storeCampaigns($campaigns, $account);
        }
    }

    /**
     * Start import campaigns by account
     *
     * @param $customer
     *
     * @return array
     */
    public function execute($customer)
    {
        $session = $this->makeSession($customer->source_account_id);

        return $this->getAccountCampaigns($session);
    }

    /**
     * Get Account Campaigns
     *
     * @param AdWordsSession $session
     *
     * @return array
     */
    private function getAccountCampaigns(AdWordsSession $session)
    {
        $campaign_service = (new AdWordsServices())->get($session, CampaignService::class);

        $selector = new Selector();
        $selector->setFields(['Id', 'Name', 'Status', 'StartDate']);

        $this->logger->info('starting to fetch account campaigns');

        return $this->executeAccountCampaigns($session, $campaign_service, $selector);
    }

    /**
     * Execute Account Campaigns
     *
     * @param AdWordsSession $session
     * @param mixed          $service
     * @param mixed          $selector
     *
     * @return array
     * @throws Exception
     */
    private function executeAccountCampaigns(AdWordsSession $session, $service, $selector)
    {
        try {
            $campaigns = $service->get($selector);
            $entries = $campaigns->getEntries();
            if (is_array($entries) && count($entries) > 0) {
                $this->logger->info('fetched ' . count($entries) . ' campaigns');

                return $entries;
            }
        } catch (Exception $exception) {
            return $this->handleAccountCampaignsOAuthException($session, $exception);
        }
    }

    /**
     * Handle Account Campaigns OAuth Exception
     *
     * @param AdWordsSession $session
     * @param Exception      $exception
     *
     * @return array
     * @throws Exception
     */
    private function handleAccountCampaignsOAuthException(AdWordsSession $session, Exception $exception)
    {
        $message = json_decode($exception->getMessage(), true);

        if ($message['error'] == 'internal_failure') {
            sleep(5);
            $this->logger->info('account campaigns exception is internal_failure, sleeping for 5 seconds, will try again.');

            return $this->getAccountCampaigns($session);
        } else {
            $this->logger->error('Unknown exception', $exception);
            throw $exception;
        }
    }

    /**
     * insert campaigns id and name to table
     *
     * @param $campaigns - must have collection
     * @param $account   - this account model
     */
    public function storeCampaigns($campaigns, $account)
    {
        if ($campaigns) {
            foreach ($campaigns as $campaign) {
                try {
                    $account_campaign = AccountCampaign::where('account_id', $account->id)
                        ->where('campaign_id', $campaign->getId())->first();

                    if (!$account_campaign) {
                        // if the campaign was not found, create it
                        $values = [
                            'account_id'        => $account->id,
                            'campaign_id'       => $campaign->getId(),
                            'campaign_name'     => $campaign->getName(),
                            'status'            => $campaign->getStatus(),
                            'segment'           => $this->getSegment($campaign->getName(), $account),
                            'start_date'        => date('Y-m-d H:i:s', strtotime($campaign->getStartDate())),
                            'source_account_id' => $account->source_account_id
                        ];

                        AccountCampaign::create($values);
                    } else {
                        $account_campaign->campaign_name = $campaign->getName();
                        $account_campaign->status        = $campaign->getStatus();
                        $account_campaign->start_date    = date('Y-m-d H:i:s', strtotime($campaign->getStartDate()));
                        $account_campaign->save();
                    }
                } catch (\PDOException $e) {
                    $this->logger->info('account campaigns failure store to table (account id: ' . $account->id . ') error: ' . $e->getMessage());
                }
            }
        }
    }

    /**
     * @Override parent method
     * @return null
     */
    public function hasData()
    {
        return null;
    }

    //abstract method - not used

    public function getFields()
    {
    }

    //abstract method - not used
    protected function setSelector()
    {
        // TODO: Implement setSelector() method.
    }

    //abstract method - not used
    protected function setReportDefinition(Selector $selector)
    {
        // TODO: Implement setReportDefinition() method.
    }
}