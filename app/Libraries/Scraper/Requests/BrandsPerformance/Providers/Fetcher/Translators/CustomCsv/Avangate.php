<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;

trait Avangate
{
    /**
     * Avangate
     * Custom translator for Avangate
     *
     * @param $csv
     * @param $offset
     *
     * @return mixed
     */
    public function avangate($csv, $offset)
    {
        $translated_result = self::translate($csv, $offset);

        foreach ($translated_result as &$translated_item) {
            if ($translated_item['Link Source'] == '-') {
                $translated_item['Link Source'] = $this->avangate_findLinkSourceByOrderRef($translated_result, $translated_item['Order Reference']);
            }
        }

        return $translated_result;
    }

    /**
     * Find Link Source By Order Reference
     * searches for item with same "Order Reference" and takes "Link Source"(token) from there
     *
     * @param $translated_result
     * @param $order_ref
     *
     * @return mixed
     */
    private function avangate_findLinkSourceByOrderRef($translated_result, $order_ref)
    {
        foreach ($translated_result as $translated_item) {
            if ($translated_item['Order Reference'] == $order_ref && $translated_item['Link Source'] != '-') {
                return $translated_item['Link Source'];
            }
        }
    }
}