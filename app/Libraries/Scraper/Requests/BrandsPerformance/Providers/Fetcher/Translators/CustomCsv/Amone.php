<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;

use App\Libraries\TpLogger;
use DB;
use Exception;
use Illuminate\Validation\Rules\In;

trait Amone
{


    /**
     * Translator for Amone Script
     * Compares tokens from the DB (out_clicks) with those we get from the CSV
     * @param $csv
     * @param $offset
     * @param $date_offset
     * @param $date_col
     * @param $date_format
     * @param $token_col
     * @param $sign
     * @param $brands_id
     * @return mixed
     */
    public function amone($csv,$offset,$date_offset,$date_col,$date_format,$token_col,$sign,$brands_id)
    {
        $translated_result = self::translate($csv, $offset);

        if(str_contains($brands_id,';')) {
            $brands_id = explode(';', $brands_id);
        }

        $date = date('Y-m-d', strtotime($date_offset));

        # Date in case the user entered wrong / too far date
        $date_for_safety = date('Y-m-d', strtotime("-1 month"));

        # Cuts the translated data according to the date offset
        $translated_result_filter = array_filter($translated_result,function($value) use($date_col,$date,$date_format){
            if(date_format(date_create_from_format($date_format,$value[$date_col]),'Y-m-d') >= $date)
                return 1;
        });

        # Get data from db
        $track_from_DB = DB::connection('dwh')->table('dwh_fact_tracks')
            ->select('id')
            ->whereIn('brand_id', $brands_id)
            ->where('click_out', '1')
            ->whereDate('updated_at', '>', $date)
            ->whereDate('updated_at', '>', $date_for_safety)->get()->toArray();

        # StdCalls to Array
        $track_from_DB = array_map(function ($row) {
            $r = json_decode(json_encode($row), true);
            return $r;
        }, $track_from_DB);

        $track_without = str_replace($sign,'',array_column($track_from_DB, 'id'));

        $track_from_DB = array_column($track_from_DB, 'id');

        # Look for the lower case values and build a new result
        $output = [];

        foreach ($translated_result_filter as $key => $row) {
            $token = $row[$token_col];
            $row[$token_col . '_original'] = $token;
            if ($token) { // If not EMPTY Token
                $index = array_search($token, $track_without);
                if ($index) {
                    $row[$token_col] = $track_from_DB[$index];
                }
            }
            $output[] = $row;
        }
        return $output;
    }
}