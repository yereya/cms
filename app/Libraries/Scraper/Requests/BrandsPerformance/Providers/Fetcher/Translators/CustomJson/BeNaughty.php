<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson;

trait BeNaughty
{
    /**
     * BeNaughty
     * after standard translate, this function checks if token is in data1 or data2
     *
     * @param      $json
     * @param null $offset
     *
     * @return mixed
     */
    public function beNaughty($json, $offset = null)
    {
        $translated_result = self::translate($json, $offset);

        if (is_array($translated_result)) {
            foreach ($translated_result as &$translated_item) {
                $translated_item['token'] = $translated_item['data2'] ?: $translated_item['data1'];
            }
        }

        return $translated_result;
    }
}