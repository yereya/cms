<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson;


trait freshSales
{
    /**
     * freshsalesTokenProcess translator
     *
     * @param      $json
     * @param null $selector
     * @param int  $offset
     * @param int  $amount_of_headers
     * @param int  $date_offset
     * @return array
     */
    public function freshsalesTokenProcess($json, $selector = null, $offset = 2, $amount_of_headers = 6, $date_offset='-4 weeks')
    {
        // Translate the relevant sheet data
//        $translated_sheet_data = self::translate($json, $selector);
        $translated_sheet_data = self::translate($json, $selector);
        $headers               = array_slice(array_values($translated_sheet_data[$offset] ?? []), 0, $amount_of_headers);
        $ISO                   = $translated_sheet_data[0][0] ?? [];
        $results = [];
        $final_result = [];

        for ($i = $offset + 1; $i < count($translated_sheet_data) - 1; $i++) {
            // Find the missing columns, and append them as empty strings by recovering the key difference array
            $_current_row       = array_slice($translated_sheet_data[$i], 0, $amount_of_headers);
            $missing_keys       = array_diff_key($headers, $_current_row);
            $complementary_data = array_fill(count($_current_row), count($missing_keys), "");
            $results[]            = array_combine($headers, $_current_row + $complementary_data) + ['iso' => $ISO];
        }



        # Translate the date_offset to date format, should be of strtotime format
        if (!strtotime($date_offset)) { // Wrong format
            throw new \Exception(" Error in the format of the date offset variable, use strtotime format - freshshalesTokenProcess.php trait.");
        }
        $date = date('Y-m-d', strtotime($date_offset));

        # Date in case the user entered wrong / too far date
        $date_for_safety = date('Y-m-d', strtotime("-3 month"));
        $brand_id = 2276;

        # Get data from db
        $track_upper_case = \DB::connection('dwh')->table('dwh_fact_tracks')
            ->select('id','source_track')
            ->where('brand_id', $brand_id)
            ->whereDate('updated_at', '>', $date)
            ->whereDate('updated_at', '>', $date_for_safety)->get()->toArray();


        # StdCalls to Array
        $track_upper_case = array_map(function ($row) {
            $r = json_decode(json_encode($row), true);
            return $r;
        }, $track_upper_case);

        $track_lower_case = array_map('strtolower', array_column($track_upper_case, 'id'));

        foreach ($results as $result){
            $row = $result;
            $index = array_search($row['Token'], $track_lower_case,true);
            if ($index) {
                $row['new_token'] = $track_upper_case[$index]['id'];
            }else{
                $row['new_token'] = '';
            }
            $final_result[] = $row;
        }

        return $final_result;
    }
}
