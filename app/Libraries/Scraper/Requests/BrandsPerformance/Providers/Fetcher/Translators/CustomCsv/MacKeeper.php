<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;

trait MacKeeper
{
    /**
     * MacKeeper
     *
     * @param        $csv
     * @param        $offset
     * @param int    $limit
     * @param string $revenue_share
     *
     * @return mixed
     */
    public function macKeeper($csv, $offset, $limit = 0, $revenue_share = '0.65')
    {
        $translated_result = self::translate($csv, $offset, $limit);

        foreach ($translated_result as &$translated_item) {
            // extract the token: 3-steps-land;bY1V0HRt
            $translated_item['token'] = substr($translated_item['Tracking ID'], strpos($translated_item['Tracking ID'], ';') + 1);

            $translated_item['revenue_share'] = $revenue_share * $translated_item['Sales(USD)'];
        }

        return $translated_result;
    }
}