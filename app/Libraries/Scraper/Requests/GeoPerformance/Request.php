<?php namespace App\Libraries\Scraper\Requests\GeoPerformance;

use App\Libraries\Scraper\Request as BaseRequest;
// Google
use App\Libraries\Scraper\Requests\GeoPerformance\Providers\Google\Provider as GoogleProvider;
use App\Libraries\Scraper\Requests\GeoPerformance\Providers\Google\Translators\GeoPerformanceReport as GoogleGeoPerformanceReport;
// Google Conversions
use App\Libraries\Scraper\Requests\GeoPerformance\Providers\Google\ConversionProvider as GoogleConversionProvider;
use App\Libraries\Scraper\Requests\GeoPerformance\Providers\Google\Translators\GeoConversionPerformanceReport as GoogleGeoConversionPerformanceReport;
// Bing
use App\Libraries\Scraper\Requests\GeoPerformance\Providers\Bing\Provider as BingProvider;
use App\Libraries\Scraper\Requests\GeoPerformance\Providers\Bing\Translators\GeoPerformanceReport as BingGeoPerformanceReport;
// Bing GNG
use App\Libraries\Scraper\Requests\GeoPerformance\Providers\BingGNG\Provider as BingGNGProvider;
use App\Libraries\Scraper\Requests\GeoPerformance\Providers\BingGNG\Translators\GeoPerformanceReport as BingGNGGeoPerformanceReport;
// Google GNG
use App\Libraries\Scraper\Requests\GeoPerformance\Providers\GoogleGNG\Provider as GoogleGNGProvider;
use App\Libraries\Scraper\Requests\GeoPerformance\Providers\GoogleGNG\Translators\GeoPerformanceReport as GoogleGNGGeoPerformanceReport;
// Google GNG Conversions
use App\Libraries\Scraper\Requests\GeoPerformance\Providers\GoogleGNG\ConversionProvider as GoogleGNGConversionProvider;
use App\Libraries\Scraper\Requests\GeoPerformance\Providers\GoogleGNG\Translators\GeoConversionPerformanceReport as GoogleGNGGeoConversionPerformanceReport;
// Google - Geo - Expanded
use App\Libraries\Scraper\Requests\GeoPerformance\Providers\GoogleGeoExpanded\Provider as GoogleGeoExpandedrovider;
use App\Libraries\Scraper\Requests\GeoPerformance\Providers\GoogleGeoExpanded\Translators\GeoExpandedPerformanceReport as GoogleGeoExpandedPerformanceReport;
// Bing - Geo - Expanded
use App\Libraries\Scraper\Requests\GeoPerformance\Providers\BingGeoExpanded\Provider as BingGeoExpandedrovider;
use App\Libraries\Scraper\Requests\GeoPerformance\Providers\BingGeoExpanded\Translators\GeoPerformanceReport as BingGeoExpandedPerformanceReport;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 38;

    /**
     * @var array $providers
     */
    protected $providers = [
        'google' => [
            'class'       => GoogleProvider::class,
            'translators' => [
                GoogleGeoPerformanceReport::class
            ]
        ],
        'google-conversion' => [
            'class'       => GoogleConversionProvider::class,
            'translators' => [
                GoogleGeoConversionPerformanceReport::class
            ]
        ],
        'googleGNG' => [
            'class'       => GoogleGNGProvider::class,
            'translators' => [
                GoogleGNGGeoPerformanceReport::class
            ]
        ],
        'googleGNG-conversion' => [
            'class'       => GoogleGNGConversionProvider::class,
            'translators' => [
                GoogleGNGGeoConversionPerformanceReport::class
            ]
        ],
        'bing'   => [
            'class'       => BingProvider::class,
            'translators' => [
                BingGeoPerformanceReport::class
            ]
        ],
        'bingGNG'   => [
            'class'       => BingGNGProvider::class,
            'translators' => [
                BingGNGGeoPerformanceReport::class
            ]
        ],
        'google-geo-expandad'   => [
            'class'       => GoogleGeoExpandedrovider::class,
            'translators' => [
                GoogleGeoExpandedPerformanceReport::class
            ]
        ],
        'bing-geo-expandad'   => [
            'class'       => BingGeoExpandedrovider::class,
            'translators' => [
                BingGeoExpandedPerformanceReport::class
            ]
        ]
    ];
}
