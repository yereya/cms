<?php namespace App\Libraries\Scraper\Requests\GenderPerformance\Providers\Google;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class ConversionProvider extends Provider
{
    /**
     * @var int $report_id
     */
    protected $report_id = 29;

    /**
     * @var string
     */
    protected $report_folder_name = 'GenderConversionsPerformance';
}