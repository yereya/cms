<?php namespace App\Libraries\Scraper\Requests\ClickPerformance\Providers\GoogleGNG;

use App\Libraries\Scraper\Providers\GoogleGNGReport;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDefinition;
use Google\AdsApi\AdWords\v201809\cm\Selector;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends GoogleGNGReport
{
    /**
     * @var string $report_type
     */
    protected $report_type = 'CLICK_PERFORMANCE_REPORT';

    /**
     * This variable is used for defining when the fetching of the reports
     * process should pull each report on it's own instead of an entire batch
     * used for adding a suffix for each report file.
     *
     * @var bool fetch_report_by_day
     */
    protected $fetch_report_by_day = true;

    /**
     * @var int
     */
    protected $report_id = 7;

    /**
     * @var string
     */
    protected $report_folder_name = 'ClickPerformance';

    /**
     * @var string
     */
    protected $store_method = 'insertOrUpdate';

    /**
     * Set Selector
     *
     * @return Selector
     */
    protected function setSelector()
    {
        $selector = new Selector;
        $selector->setFields(array_unique(array_flatten($this->getFields())));

        return $selector;
    }

    /**
     * Set Report Definition
     *
     * @param Selector $selector
     *
     * @return ReportDefinition
     */
    protected function setReportDefinition(Selector $selector)
    {
        $reportDefinition                 = new ReportDefinition();
        $reportDefinition->setSelector($selector);
        $reportDefinition->setReportName('Click Performance Report #' . uniqid());
        $reportDefinition->setDateRangeType($this->date_range_type); //today and yesterday
        $reportDefinition->setReportType($this->report_type);
        $reportDefinition->setDownloadFormat('CSV');

        return $reportDefinition;
    }
}
