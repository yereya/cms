<?php namespace App\Libraries\Scraper\Requests\InvocaReport\Providers\Invoca;

use App\Libraries\Scraper\Providers\InvocaReport;


/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends InvocaReport
{
    /**
     * @var int
     */
    protected $report_id = 28;

    /**
     * @var string $report_type
     */
    protected $report_type = 'INVOCA_REPORT';

    /**
     * @var string $report_request
     */

    /**
     * @var string
     */
    protected $report_folder_name = 'InvocaReport';

    /**
     * @var string
     */
    protected $store_method = 'insertOrUpdate';

}