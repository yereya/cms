<?php namespace App\Libraries\PublisherEditor\Providers;

use App\Libraries\PublisherEditor\Operations\Constants;
use BingAds\Proxy\ClientProxy;
use Cache;
use GuzzleHttp\Client;

class Bing
{
    /**
     * @var string $refresh_token
     */
    protected $refresh_token;

    /**
     * @var string $access_token
     */
    protected $access_token;

    /**
     * @var Client $client
     */
    protected $client;

    /**
     * @var ClientProxy $proxy
     */
    protected $proxy;

    /**
     * @var Account Id - source account id
     */
    private $account_id;

    /**
     * Bing constructor.
     *
     * @param $source_account_id
     * @param $wsdl_type
     */
    public function __construct($source_account_id, $wsdl_type = null)
    {
        $this->client = new Client;
        $this->setRefreshToken();
        $this->setAccessToken();
        $this->account_id = $source_account_id;
        $this->setProxy(Constants::BING_URL_CAMPAIGN_MANAGER);
    }

    /**
     * @return $this - this object
     */
    public function get()
    {
        return $this;
    }

    /**
     * Getter account id
     *
     * @return source account id
     */
    public function getAccountId()
    {
        return $this->account_id;
    }

    /**
     * Get Records
     *
     * @param $operations
     * @param $request
     *
     * @return mixed
     * @throws \Exception
     */
    public function getRecords($operations, $request)
    {
        try {
            $records = $this->proxy->GetService()->$operations($request);
        } catch (\SoapFault $e) {
            if (isset($e->detail->ApiFaultDetail)) {
                throw new \Exception($e->detail->ApiFaultDetail->OperationErrors);
            } else {
                throw new \Exception($e->getMessage());
            }
        }

        return $records;
    }

    /**
     * Set Refresh Token
     */
    private function setRefreshToken()
    {
        $this->refresh_token = Cache::get('bing-ads.refresh_token') ?: config('bing-ads.refresh_token');
    }

    /**
     * Set Access Token
     */
    private function setAccessToken()
    {
        $request = $this->client->get($this->getRefreshTokenUrl());
        $response = json_decode($request->getBody()->getContents(), true);

        $this->access_token = $response['access_token'];
        $this->refresh_token = $response['refresh_token'];

        Cache::forever('bing-ads.refresh_token', $this->refresh_token);
    }

    /**
     * Get Refresh Token URL
     *
     * @return string
     */
    private function getRefreshTokenUrl()
    {
        $url = 'https://login.live.com/oauth20_token.srf?';
        $url .= 'client_id=' . config('bing-ads.client_id');
        $url .= '&refresh_token=' . $this->refresh_token;
        $url .= '&grant_type=refresh_token';
        $url .= '&redirect_uri=https%3A%2F%2Flogin.live.com%2Foauth20_desktop.srf';

        return $url;
    }

    /**
     * Set Proxy
     *
     * @param string $wsdl
     *
     * @return ClientProxy
     */
    protected function setProxy($wsdl)
    {
        $this->proxy = ClientProxy::ConstructWithAccountAndCustomerId(
            $wsdl,
            config('bing-ads.user_name'),
            config('bing-ads.password'),
            config('bing-ads.developer_token'),
            $this->account_id,
            config('bing-ads.customer_id'),
            $this->access_token
        );
    }
}