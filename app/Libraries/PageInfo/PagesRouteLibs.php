<?php

namespace App\Libraries\PageInfo;

use App\Entities\Models\Sites\Page;
use App\Entities\Models\Sites\Post;
use Cache;
use Illuminate\Support\Collection;

class PagesRouteLibs
{
    /**
     * @var $pages Collection|null
     */
    protected $pages;

    /**
     * @var $posts Collection|null
     */
    protected $posts;

    /**
     * constants page type
     */
    const PAGE_TYPE_STATIC = 'static';
    const PAGE_TYPE_ARCHIVE = 'archive';
    const CACHE_NAME = 'pages_url_';
    const CACHE_TIME = 2;

    /**
     * PageRepository constructor.
     */
    public function __construct()
    {
        $this->setPages();
        $this->setPosts();
    }

    /**
     * Get Pages
     *
     * @return mixed
     */
    protected function getPages(): Collection
    {
        return $this->pages;
    }

    /**
     * Get Posts
     *
     * @param int|null $content_type_id
     *
     * @return mixed
     */
    protected function getPosts(int $content_type_id = null): Collection
    {
        if ($content_type_id) {
            return $this->posts->filter(function ($post) use ($content_type_id) {
                return $post->content_type_id == $content_type_id;
            });
        }

        return $this->posts;
    }

    /**
     * Set Pages
     */
    private function setPages()
    {
        $this->pages = Page::with('pageContentTypes', 'post')->get()->keyBy('post_id');
    }

    /**
     * Set Posts
     */
    private function setPosts()
    {
        $this->posts = Post::all();
    }

    /**
     * Return posts by type
     *
     * @param string $type
     *
     * @return mixed
     */
    protected function getByType(string $type = self::PAGE_TYPE_STATIC) :Collection
    {
        $pages = Cache::get(self::CACHE_NAME . $type);

        return $pages ?? $this->getPagesByType($type);
    }

    /**
     * Find pages by type and insert to cache
     *
     * @param $type
     *
     * @return mixed
     */
    private function getPagesByType(string $type) :Collection
    {
        $pages = $this->getPages()->filter(function ($page) use ($type) {
            return $page->type == $type && $page->post;
        });

        $this->savePagesToCache($pages, $type);

        return $pages;
    }

    /**
     * Save to cache
     *
     * @param $pages
     * @param $type
     */
    private function savePagesToCache(Collection $pages, string $type)
    {
        Cache::put(self::CACHE_NAME . $type, $pages, self::CACHE_TIME);
    }
}