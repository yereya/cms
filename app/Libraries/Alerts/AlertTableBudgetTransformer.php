<?php namespace App\Libraries\Alerts;


/**
 * Class AlertTableBudgetTransformer
 * @package App\Libraries\Alerts
 */
class AlertTableBudgetTransformer implements AlertTableTransformerInterface
{
    /**
     * Transform budget array to a predictable structure
     *
     * @param $data
     * @param $date
     *
     * @return array
     */
    public static function transform($data, $date)
    {
        //format each item so it contains predictable table columns
        return array_map(function ($item) {
            return [
                'stats_date_tz'   => $item['stats_date_tz'],
                'budget_id'       => $item['budget_id'],
                'budget_name'     => $item['budget_name'],
                'budget'          => $item['budget'],
                'cost'            => $item['cost'],
                'usage'           => $item['usage'],
                'account_name'    => $item['account_name'],
                'account_id'      => $item['account_id'],
                'advertiser_name' => $item['advertiser_name'],
                'exceed'          => $item['exceed'],
                'update_hour'     => $item['update_hour'],
            ];
        }, $data);

    }

}