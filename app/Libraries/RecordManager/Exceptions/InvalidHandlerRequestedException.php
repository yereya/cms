<?php namespace App\Libraries\RecordManager\Exceptions;

/**
 * Class InvalidHandlerRequestedException
 *
 * @package App\Libraries\RecordManager\Exceptions
 */
class InvalidHandlerRequestedException extends \Exception
{
    //
}