<?php namespace App\Libraries\RecordManager\Records;



use App\Libraries\Bo\OutInterfaces\ScraperLib;
use Illuminate\Database\Connection;

class AdvertiserTracksConnection extends BaseConnection
{
    /**
     * key - value for connection inputs
     */
    const CONNECTIONS_MAP = [
        0 => 'out',
        1 => 'putin'
    ];

    /**
     * Determines connection by
     *
     * @param $input_connection
     * @return Connection
     * @throws \Exception
     */
    public function determineConnectionByInput($input_connection) : Connection
    {
        $connection_name = self::CONNECTIONS_MAP[$input_connection];

        if (!$this->connectionExists($connection_name)) {
            throw new \Exception("Invalid connection $connection_name.");
        }

        $this->resetConnectionTo($connection_name);

        return $this->to_connection;
    }

    /**
     * Triggers the scraper event with its values
     *
     * @param array $values
     */
    public function triggerScraperEvent(array $values)
    {
        $this->logger->info(['']);
        $scraper_lib = new ScraperLib();

        $response_result = $scraper_lib->create($values);

        return $response_result['data']->data ?? 0;
    }
}