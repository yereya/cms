<?php namespace App\Libraries\RecordManager\Records\Requests;

use Illuminate\Support\Facades\Request;

/**
 * Class RecordManageRequest
 *
 * @package App\Http\Requests
 */
class TracksManagerRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|alpha_dash|unique:users,username',
            'first_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'min:8',
            'password_repeat' => 'required_with:password|same:password',
            'password_expiry_date' => 'required|date'
        ];
    }

}