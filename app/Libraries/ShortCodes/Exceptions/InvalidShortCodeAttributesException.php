<?php namespace App\Libraries\ShortCodes\Exceptions;

/**
 * Class InvalidShortCodeAttributesException
 *
 * @package App\Libraries\ShortCodes\Exceptions
 */
class InvalidShortCodeAttributesException extends \Exception
{
    //
}