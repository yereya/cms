<?php

namespace App\Libraries\ActionButton;

use App\Libraries\ActionButton\Exceptions\InvalidActionButtonTypeException;
use App\Libraries\ActionButton\Exceptions\MissingActionButtonLinkTypeUrlException;
use Illuminate\Support\Facades\View;

/**
 * Class HtmlActions for rendering and parsing dynamic html buttons
 *
 * @package app\Libraries\Datatable
 */
class ActionButtonLib
{

    /**
     * An array of buttons, each button contains its specific attributes
     *
     * @var array
     */
    private static $buttons_args= [];


    /**
     * Required user Permission code name for performing specific action
     *
     * @var
     */
    private static $permission_name;

    /**
     * Array of additional data which the buttons elements should be aware of
     *
     * @var
     */
    private static $params;

    /**
     * parse and render html actions
     *
     * @param $buttons_args
     * @param $permission_name
     * @param $params
     *
     * @return string
     */
    public static function render(array $buttons_args = [], $permission_name = '', array $params = [])
    {
        // save actions,permissions_name & params to a static attribute
        // so it can be accessed from other static methods
        self::$buttons_args = $buttons_args;
        self::$permission_name = $permission_name;
        self::$params = $params;

        return self::parseActions();
    }

    /**
     * loop through given actions and concatenate their html results into one html string
     *
     * @return string
     */
    private static function parseActions()
    {
        $html_response = '';

        //go through each action and parse it according to its type
        foreach (self::$buttons_args as $button) {

            $action_response = self::parseAction($button);

            if ($action_response) {
                $html_response .= $action_response;
            }
        }

        return $html_response;
    }


    /**
     * parse current given action
     *
     * @param $button
     *
     * @return bool
     * @throws InvalidActionButtonTypeException
     */
    private static function parseAction($button)
    {
        //Check if user is permitted to perform the current action
        if (!self::isUserPermitted($button)) {
            return false;
        }

        // if action type is supported then continue parsing according to method convention
        $method_name = $button['type'] . 'Type';
        if (!method_exists(__CLASS__, $method_name)) {
            throw new InvalidActionButtonTypeException($method_name . ' static method is not supported or does not exist.');
        }

        return self::$method_name($button);
    }


    /**
     * check if user is permitted to perform given action
     *
     * @param $button
     *
     * @return bool
     */
    private static function isUserPermitted($button)
    {
        if (!isset($button['permissions'])) {
            return true;
        }

        return auth()->user()->can(self::$permission_name . "." . $button['permissions']);
    }

    /**
     * render delete type action button
     *
     * @param $button
     *
     * @return mixed
     */
    private static function deleteType($button)
    {
        return View::make('partials.containers.buttons.delete', [
            'route' => [$button['route'], self::$params['id']],
            'title' => ucfirst($button['name']),
            'attributes' => ucfirst($button['name'])
        ])->render();
    }


    /**
     * render alert type action button
     *
     * @param $button
     *
     * @return mixed
     */
    private static function alertType($button)
    {
        $route = self::buildRoute($button);

        return View::make('partials.containers.buttons.alert', [
            'route' => $route,
            'title' => $button['name'],
            'text' => isset($button['text']) ? $button['text'] : '',
            'icon' => $button['icon'],
            'type' => isset($button['alertType']) ? $button['alertType'] : 'info'
        ])->render();
    }


    /**
     * render modal type action button
     *
     * @param $button
     *
     * @return mixed
     */
    private static function modalType($button)
    {
        $route = self::buildRoute($button);

        return View::make('partials.containers.buttons.modal', [
            'route' => $route,
            'title' => $button['name'],
            'icon' => $button['icon']
        ])->render();
    }


    /**
     * render link type action button
     *
     * @param $button
     *
     * @return mixed
     * @throws MissingActionButtonLinkTypeUrlException
     */
    private static function linkType($button)
    {
        if (isset($button['route'])) {
            $route = self::buildRoute($button);
        } elseif (isset($button['url'])) {
            $route = url($button['url'] . self::$params['id']);
        } else {
            throw new MissingActionButtonLinkTypeUrlException();
        }

        return View::make('partials.containers.buttons.link', [
            'route' => $route,
            'title' => ucfirst($button['name']),
            'icon' => $button['icon']
        ])->render();
    }

    /**
     * Build the relevant route
     *
     * @param $button
     *
     * @return string
     */
    private static function buildRoute($button)
    {
        if (isset($button['route_params']) && $button['route_params']) {
            return route($button['route'], [self::$params['id'], $button['route_params']]);
        }

        return route($button['route'], [self::$params['id']]);
    }

}