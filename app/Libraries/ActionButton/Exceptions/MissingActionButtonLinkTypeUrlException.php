<?php namespace App\Libraries\ActionButton\Exceptions;

/**
 * Class MissingActionButtonLinkTypeUrlException
 *
 * @package App\Libraries\ActionButto\Exceptions
 */
class MissingActionButtonLinkTypeUrlException extends \Exception
{
    //
}