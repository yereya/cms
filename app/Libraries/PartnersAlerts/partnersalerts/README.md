# PartnersAlerts
Monitor third party endpoints of high PPC traffic using Pingdom.

### Installation
Create `.env` file, see `.env.example` for more information.

Install the dependencies and start the app.

```sh
$ git clone {this-repo}
$ npm install
$ npm run start
```