<?php namespace

App\Libraries\Dashboards\Controllers;

use App\Entities\Repositories\Dwh\DashDailyStatsRepo;
use Illuminate\Http\Request;

/**
 * Class DashStats
 *
 * @package App\Libraries\Dashboards\Controllers
 */
class SummaryStats extends Dashboards
{

    /**
     * Dash stats
     *
     * @var $stats
     */
    protected $stats;


    /**
     * @var DashDailyStatsRepo
     */
    protected $dash_daily_stats_repo;

    /**
     * DashStats constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->dash_daily_stats_repo = (new DashDailyStatsRepo());
    }

    /**
     * Build Data
     *
     * @return mixed
     */
    public function build()
    {
        if ($this->period == 'daily') {
            $stats     = $this->dash_daily_stats_repo->getYesterday();
            $ratio = $this->dash_daily_stats_repo->getLastTwoWeeksPercentageRatio();
        } else {
            $stats = $this->dash_daily_stats_repo->getLastMonth();
            $ratio = $this->dash_daily_stats_repo->getLastTwoMonthsPercentageRatio();
        }
        if (!empty($stats)) {
            $stats          = $this->formatCurrency($stats[0]);
            $stats['ratio'] = $ratio;
            $this->stats    = $stats;
            return;
        }

        $this->stats = [];
    }

    /**
     * Pass Data To View
     *
     * @return mixed
     */
    public function get()
    {
        return $this->stats;
    }

    /**
     * extend the formatCurrency Helper
     * to pass it  array of params
     *
     * @param $sum
     *
     * @return array
     */
    private function formatCurrency($sum)
    {
        if (is_array($sum)) {
            $sum = array_map(function ($value) {
                return formatCurrency($value, 0);
            }, $sum);
        } else {
            return formatCurrency($sum);
        }
        return $sum;
    }
}