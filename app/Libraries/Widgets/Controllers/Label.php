<?php

namespace App\Libraries\Widgets\Controllers;

use App\Entities\Models\Sites\Post;
use App\Entities\Repositories\Sites\LabelRepo;
use App\Entities\Repositories\Sites\PostRepo;
use App\Libraries\Widgets\Widgets;
use App\Entities\Repositories\Sites\Widgets\WidgetDataPostRepo;
use App\Entities\Models\Sites\Post as PostModel;
use Illuminate\Support\Collection;

/**
 * Class Article
 *
 * @package App\Libraries\WidgetsBuilder\Widgets
 */
class Label extends Widgets
{
    /**
     * @var
     */
    private $labels;

    /**
     * @var LabelRepo
     */
    private $label_repo;

    /**
     * Verify
     *
     * @return bool
     */
    public function verify()
    {
        return true;
    }

    /**
     * Build
     *  1. Get label group from request.
     *  2. Check if it passed:
     *   - Get the children of the passed labels.
     *  3. If not passed:
     *   - If specific post pass - get the labels for this post
     *   - else Get all labels from current post.
     */
    public function build()
    {
        $this->post_repo  = new PostRepo();
        $this->label_repo = new LabelRepo();
        $this->resolvePost();
        $label_group      = $this->widget_data_attributes['labels'] ?? null;

        if (!empty($label_group->ids) && is_array($label_group->ids)) {
            $this->labels = $this->resolvePostLabels($label_group->ids, $this->post);
        } else {
            $this->labels = $post->labels;
        }
    }

    /**
     * Get
     *
     * @return string
     */
    public function get()
    {
        return view($this->getViewPath())
            ->with('labels', $this->labels)
            ->render();
    }


    /**
     * Get Post
     *
     * @return PostModel|null
     */
    private function resolvePost()
    {
        $post_order = (new WidgetDataPostRepo())->getByWidgetDataId($this->widget_data->id);

        if (!empty($post_order->post_id)) {
            $this->post = $this->post_repo->find($post_order->post_id);
        } else {
            if (!empty($this->current_post) && $this->current_post->isAPage()) {
                $this->post = $this->current_post;
            } else {
                $content_type_id = $this->widget_data->content_type_id;
                $this->post      = $this->post_repo->getFirstPostByContentTypeId($content_type_id);
            }
        }
    }

    /**
     * Get Labels From Array Of Posts
     *
     * @param array     $ids
     * @param PostModel $post
     *
     * @return Collection
     * @internal param $post_id
     * @internal param PostModel $post
     * @internal param array $labels
     * @internal param $label_group
     * @internal param $post_id
     */
    public function resolvePostLabels(array $ids, PostModel $post): Collection
    {
        $collection       = collect();
        $active_label_ids = $post->labels->pluck('id')->toArray();

        foreach ($ids as $key => $id) {
            $children   = $this->label_repo->getChildrenById($id);
            $this->label_repo->assignActivePost($children, $active_label_ids);
            $collection = $collection->merge($children);
        }

        return $collection;
    }
}
