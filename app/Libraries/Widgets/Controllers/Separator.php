<?php
namespace App\Libraries\Widgets\Controllers;

use App\Entities\Models\Sites\MenuItem;
use App\Libraries\Widgets\Widgets;
use Doctrine\Common\Collections\Collection;

/**
 * Class Image
 *
 * @package App\Libraries\TemplateBuilder\Widgets
 */
class Separator extends Widgets
{
    /**
     * Verify
     *
     * @return bool
     */
    public function verify()
    {
        return true;
    }

    /**
     * Build
     */
    public function build()
    {
    }

    /**
     * Get
     *
     * @return string
     */
    public function get()
    {
        return view($this->getViewPath())
            ->render();
    }
}