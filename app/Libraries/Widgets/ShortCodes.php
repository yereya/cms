<?php

namespace App\Libraries\Widgets;

use App\Libraries\PageInfo\PagesUrls;
use App\Libraries\ShortCodes\Contracts\ShortCodes as ShortCodesContract;

class ShortCodes implements ShortCodesContract{

    /**
     * @var array|string $haystack
     */
    protected $haystack;


    /**
     * Set Haystack
     *
     * @param array|string $haystack
     */
    public function setHaystack($haystack)
    {
        $this->haystack = $haystack;
    }

    /**
     * Generate url by post id and page post id.
     *
     * @param Array $attributes
     *
     * @return mixed
     */
    public function sc_href($attributes){
        $attributes = filterAndSetParams([
            'page_post_id' => -1,
            'post_id' => -1
        ], $attributes);

        $pagesUrls = app(PagesUrls::class);

        return $pagesUrls->getHref($attributes['page_post_id'],$attributes['post_id']);
    }

    /**
     * Runs php date function
     *
     * @param array $atts short code standard attributes
     *
     * @return string
     * @throws \Exception
     */
    public function sc_date($atts)
    {
        $atts = filterAndSetParams([
            'format'     => 'Y-m-d',
            // the php format for the date. http://php.net/manual/en/function.date.php
            'span'       => null,
            // Same as the params accepted in strtotime(). http://php.net/manual/en/function.strtotime.php. Ex: "+1 week", "last Monday"
            'col_name'   => null,
            'col_number' => null
        ], $atts);

        $now = time();

        // Try parsing the date
        if ($atts['col_name'] OR $atts['col_number']) {
            if (($timestamp = strtotime($this->sc_var($atts))) !== false) {
                $now = $timestamp;
            } else {
                throw new \Exception("** Fatal error: could not translate column value (". $this->sc_var($atts) .") using strtotime(). " . __CLASS__ . '@' . __FUNCTION__ . '():' . __LINE__ ." ". json_encode($atts));
            }
        }

        if (isset($atts['span']) && ($timestamp = strtotime($atts['span'], $now)) !== false) {
            $now = $timestamp;
        }

        return date($atts['format'], $now);
    }
}