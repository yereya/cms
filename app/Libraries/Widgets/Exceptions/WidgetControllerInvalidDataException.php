<?php namespace App\Libraries\Widgets\Exceptions;

/**
 * Class WidgetControllerInvalidDataException
 *
 * @package App\Libraries\TemplateBuilder\Exceptions
 */
class WidgetControllerInvalidDataException extends \Exception
{
    //
}