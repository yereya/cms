<?php


namespace App\Entities\Models;


class UserActionFeedback extends Model
{
    /**
     * @var bool $timestamps
     */
    public $timestamps = true;

    /**
     * @var array $fillable
     */
    protected $fillable = ['record_id', 'user_id', 'description', 'meta', 'state'];

    protected $table = 'user_action_feedback';

    protected $primaryKey = 'record_id';
}