<?php namespace App\Entities\Models\Bo\Changes;

use App\Entities\Models\Model;

class Type extends Model
{

    /**
     * @var string $connection
     */
    protected $connection = 'bo';

    /**
     * @var string
     */
    protected $table = 'changes_types';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'name',
    ];
}