<?php namespace App\Entities\Models\Bo;

use App\Entities\Models\Model;

class Advertiser extends Model
{

    /**
     * @var string $connection
     */
    protected $connection = 'bo';

    /**
     * @var string
     */
    protected $table = 'advertisers';

    /**
     * @var array $fillable
     */
    protected $fillable = ['name', 'status', 'type', 'domain', 'time_zone', 'mongodb_id', 'category', 'product_name'];

    /**
     * Accounts
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function accounts()
    {
        return $this->hasMany(Account::class);
    }
}