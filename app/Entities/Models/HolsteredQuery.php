<?php namespace App\Entities\Models;

use App\Entities\Models\Model;
use DB;

/**
 * Class Holstered Query
 *
 * @package App\Entities\Models\Reports
 */
class HolsteredQuery extends Model
{

    /**
     * @var bool $timestamps
     */
    public $timestamps = true;

    /**
     * @var string $table
     */
    protected $table = 'holstered_queries';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'name',
        'query',
        'db',
        'statement',
        'system_id',
    ];


    /**
     * Systems
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function systems() {
        return $this->belongsTo(System::class, 'system_id', 'id');
    }
}