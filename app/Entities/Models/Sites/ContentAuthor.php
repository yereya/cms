<?php

namespace App\Entities\Models\Sites;


class ContentAuthor extends SiteModel
{


    /**
     * @var array $fillable
     */
    protected $fillable = ['first_name', 'last_name', 'title', 'description', 'image_id'];

    /**
     * @return mixed
     */
    public function image()
    {
        return $this->hasOne(Media::class, 'id', 'image_id');
    }

    /**
     * Get Full Name Attribute
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }
}
