<?php namespace App\Entities\Models\Sites\Widgets;

use App\Entities\Models\Model;
use App\Entities\Models\Sites\SitesModel;

/**
 * App\Entities\Models\Widgets\Field

*
 * @property-read SiteWidget $widget
 * @property-read FieldType  $type
 * @property integer         $id
 * @property integer         $widget_id
 * @property integer         $widget_field_type_id
 * @property string          $title
 * @property string          $default
 * @property string          $help
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Sites\Widgets\Field whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Sites\Widgets\Field whereWidgetId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Sites\Widgets\Field whereWidgetFieldTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Sites\Widgets\Field whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Sites\Widgets\Field whereDefault($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Sites\Widgets\Field whereHelp($value)
 */
class Field extends SitesModel
{
    /**
     * @var bool $timestamps
     */
    public $timestamps = false;

    /**
     * @var string $table
     */
    protected $table = 'site_widget_fields';

    protected $fillable = [
        'widget_id',
        'type_id',
        'title',
        'default',
        'help',
        'permission'
    ];

    /**
     * Widget
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function widget()
    {
        return $this->belongsTo(SiteWidget::class, 'widget_id');
    }

    /**
     * Type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(FieldType::class, 'type_id');
    }
}