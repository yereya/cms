<?php namespace App\Entities\Models\Sites\Templates;

use App\Entities\Models\Sites\ABTest;
use App\Entities\Models\Sites\SiteModel;


class Row extends SiteModel
{


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'template_rows';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'html_element_id',
        'html_class',
        'html_container_class',
        'html_wrapper_class',
        'order',
        'total_size',
        'locked',
        'active',
        'ab_tests_action'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'locked' => 'boolean',
        'active' => 'boolean'
    ];

    /**
     * Template
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function template()
    {
        return $this->belongsTo(Template::class);
    }

    /**
     * Columns
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function columns()
    {
        return $this->hasMany(Column::class);
    }

    /**
     * Item
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function item()
    {
        return $this->morphOne(Item::class, 'element');
    }

    /**
     * AB Tests
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function abTests()
    {
        return $this->belongsToMany(ABTest::class, 'ab_test_row', 'row_id', 'test_id');
    }
}