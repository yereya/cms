<?php

namespace App\Entities\Models\Mrr;

use App\Entities\Models\Users\User;
use Illuminate\Database\Eloquent\Model;

class MrrChangeLogHistory extends Model
{
    /**
     * @var string - connection name
     */
    protected $connection = 'mrr';

    /**
     * @var string - table name
     */
    protected $table = 'mrr_change_log_history';

    protected $fillable = [
        'report_id',
        'report_name',
        'advertiser_id',
        'advertiser_name',
        'publisher_id',
        'publisher_name',
        'account_id',
        'account_name',
        'campaign_id',
        'campaign_name',
        'campaign_status',
        'campaign_budget',
        'campaign_device_bid_adjustment',
        'device',
        'ad_group_id',
        'ad_group_name',
        'ad_group_status',
        'ad_group_bid',
        'ad_group_device_bid_adjustment',
        'match_type',
        'keyword_id',
        'keyword_name',
        'keyword_status',
        'keyword_bid',
        'ad',
        'change_type',
        'change_level',
        'old_value',
        'new_value',
        'user_id',
        'comment',
        'reason',
        'expected_result',
        'follow_up_date',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
