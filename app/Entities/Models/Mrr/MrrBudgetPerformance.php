<?php

namespace App\Entities\Models\Mrr;

use Illuminate\Database\Eloquent\Model;

class MrrBudgetPerformance extends Model
{
    /**
     * @var string
     */
    protected $connection = 'mrr';

    /**
     * @var string
     */
    protected $table = 'mrr_budget_performance';
}
