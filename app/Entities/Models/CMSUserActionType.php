<?php

namespace App\Entities\Models;
use Illuminate\Database\Eloquent\Model;

class CMSUserActionType extends Model
{
    /**
     * @var string $connection
     */
    protected $connection = 'cms';

    /**
     * @var string
     */
    protected $table = 'cms_user_action_type';

    /**
     * @var array $fillable
     */
    protected $fillable = ['type'];
}
