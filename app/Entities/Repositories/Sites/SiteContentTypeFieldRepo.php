<?php

namespace App\Entities\Repositories\Sites;

use App\Entities\Models\Sites\ContentTypes\SiteContentType;
use App\Entities\Models\Sites\ContentTypes\SiteContentTypeField;
use App\Entities\Repositories\Repository;

class SiteContentTypeFieldRepo extends Repository
{
    const MODEL = SiteContentTypeField::class;

    /**
     * getFieldsById
     *
     * @param array $post_field_ids
     *
     * @return mixed
     */
    public function getFieldsById(array $post_field_ids)
    {
        return $this->model()
            ->whereIn('id', $post_field_ids)
            ->get();
    }

    /**
     * getFieldsById
     *
     * @param array $post_field_ids
     *
     * @return mixed
     */
    public function getFieldsByIdWithType(array $post_field_ids)
    {
        return $this->model()
            ->whereIn('id', $post_field_ids)
            ->get()->each(function ($field) {
                $field->display_name = $field->display_name . '[' . $field->type . ']';
            });
    }

    /**
     * Get fields list
     *
     * @param $field_ids
     *
     * @return mixed
     */
    public function getFieldsGroupBy (array $field_ids) {
        if (is_null($field_ids)) {
            return [];
        }

        $fields = [];
        foreach ($field_ids as $id) {
            $fields[] = $id->id;
        }

        return $this->model()
            ->select('display_name', 'id', 'field_group_id', 'name')
            ->whereIn('id', $fields)
            ->with('fieldGroup')
            ->get()
            ->groupBy('field_group_id')
            ->toArray();
    }

    /**
     * Calculate the next fields' priority
     *
     * @param SiteContentType $content_type
     *
     * @return int
     */
    public function getCalculatedFieldPriority(SiteContentType $content_type): int
    {
        $highest_priority_field = $this->model()
            ->where('type_id', $content_type->id)
            ->orderBy('priority', 'desc')
            ->first();

        if (count($highest_priority_field)) {
            return $highest_priority_field->priority + 1;
        }

        return 1;
    }

    /**
     * Get field type option
     *
     * @param $field
     *
     * @return string
     */
    public function getFieldType(SiteContentTypeField $field)
    {
        switch ($field->type) {
            case 'wysiwyg':
            case 'textarea':
            case 'checkboxes':
            case 'content_type_link':
            case 'select':
                $field_type = 'TEXT';
                break;
            case 'checkbox':
                $field_type = 'TINYINT(1)';
                break;
            case 'number':
                $field_type = 'INT';
                break;
            case 'phone_number':
                $field_type = 'VARCHAR(50)';
                break;
            case 'url':
                $field_type = 'VARCHAR(1000)';
                break;
            case 'currency':
                $field_type = 'VARCHAR(255)';
                break;
            default:
                $field_type = 'VARCHAR(255)';
                break;
        }

        return $field_type;
    }

    /**
     * @param $field_id
     *
     * @return null|String
     */
    public function getTypeById($field_id)
    {
        $field = $this->find($field_id);

        if ($field) {
            return $field->type;
        }

        return null;
    }

    /**
     * Get linked Field By Content Type Id And Field Name
     *
     * @param $content_type_id_link
     * @param $field_name
     *
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function getLinkedFieldByContentTypeIdAndFieldName($content_type_id_link, $field_name)
    {
        return $this->query()
            ->where('type_id', $content_type_id_link)
            ->where('name', $field_name)
            ->first();
    }
}
