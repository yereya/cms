<?php

namespace App\Entities\Repositories\Sites\Widgets;

use App\Entities\Models\Sites\Templates\Column;
use App\Entities\Models\Sites\Templates\Item;
use App\Entities\Models\Sites\Templates\WidgetData;
use App\Entities\Repositories\Repository;
use Illuminate\Database\Eloquent\Collection;
use App\Entities\Models\Sites\Widgets\Widget;
use App\Libraries\Widgets\WidgetForm;
use Illuminate\Http\Request;

class WidgetDataRepo extends Repository
{
    const MODEL = WidgetData::class;

    const WIDGET_GROUP_ID = 16;

    /**
     * getAllScss
     *
     * @return mixed|string
     */
    public function getAllSiteScss()
    {
        $used_widgets = WidgetData::with([
            'templateRowColumnItem' => function ($query) {
                $query->where('element_type', 'widget')
                    ->groupBy('element_id');
            }
        ])
            ->get()
            ->pluck('widget_id');

        $widget_data = WidgetData::whereIn('widget_id', $used_widgets)
            ->whereNotNull('scss')
            ->get();

        return $widget_data->pluck('scss')->implode('');
    }

    /**
     * Get Widget Data Connected To Post
     *
     * @param $post
     *
     * @return array
     */
    public function getWidgetDatasConnectedToPost($post)
    {
        $widget_data = $this->getWidgetDataByPost($post);

        if ($widget_data->isEmpty()) {

            return [];
        }

        return $widget_data;
    }

    /**
     * Return dynamic list by widget data id
     *
     * @param $widget_data_id
     *
     * @return null
     */
    public function getDynamicList($widget_data_id)
    {
        $widget_data = $this->find($widget_data_id);

        if ($widget_data) {
            return $widget_data->dynamicList;
        }

        return null;
    }

    /**
     * Get Widget Data
     *
     * @param $post
     *
     * @return Collection
     */
    private function getWidgetDataByPost($post)
    {
        $widget_data_rows = WidgetData::whereHas('posts', function ($query) use ($post) {
            $query->where('post_id', $post->id);
        })
            ->get();

        return $widget_data_rows;
    }

    /**
     * getItemByColumnId
     *
     * @param WidgetData $widget_data
     * @param int        $column_id
     *
     * @return Item
     */
    public function getItemByColumnId(WidgetData $widget_data, int $column_id): Item
    {
        $widget_data->with([
            'widgetDataItems' => function ($query) use ($column_id) {
                $query->where('column_id', $column_id);
            }
        ]);

        return $widget_data->widgetDataItems;
    }

    /**
     * getItemByColumn
     *
     * @param WidgetData $widget_data
     * @param Column     $column
     *
     * @return Item
     */
    public function getItemByColumn(WidgetData $widget_data, Column $column): Item
    {
        return $this->getItemByColumnId($widget_data, $column->id);
    }

    /**
     * storeFromRequest
     *
     * @param Widget  $widget
     * @param Request $request
     *
     * @return WidgetData|\Illuminate\Database\Eloquent\Model
     */
    public function storeFromRequest(Widget $widget, Request $request): WidgetData
    {
        $model = $this->model();
        $model->fill($request->only([
            'widget_id',
            'name',
            'active',
            'scss',
            'widget_template_id',
            'content_type_id',
            'menu_id'
        ]));
        $model->data = WidgetForm::getFormSavedFields($widget, $request);

        $model->save();

        return $model;
    }

    /**
     * @param $post
     *
     * @return bool
     */
    public function isPostUsedByWidgets($post)
    {
        return $this->getWidgetDataByPost($post)
            ->isEmpty();
    }

    public function updateFromRequest(WidgetData $widget_data, Request $request)
    {
        $widget_data->fill($request->all());
        $widget_data->data = WidgetForm::getFormSavedFields($widget_data->widget, $request);

        return $widget_data->save();
    }

    /**
     * Return widget list by widget type
     *
     * @param array $where
     *
     * @return mixed
     */
    public function getWidgetByTypeToSelect(array $where)
    {
        $query = $this->query();

        $query->select('id', 'name as text');

        foreach ($where as $clause) {
            if (count($clause) == 3) {
                $query->where($clause[0], $clause[1], $clause[2]);
            }
        }

        return $query->get();
    }

    /**
     * @param $scss
     * @param $class_name
     *
     * @return mixed
     */
    public function replaceTempScssClass($scss, $class_name)
    {

        return str_replace(WidgetData::PLACEHOLDER_CLASS, $class_name, $scss);
    }

    /**
     * Connect to widget data type groups - other widgets
     *
     * @param $widget_data
     * @param $request_parameters
     */
    public function storeWidgetTab($widget_data, $request_parameters)
    {
        $groups = [];

        if ($request_parameters['widget_id'] == self::WIDGET_GROUP_ID) {

            // create priority to row
            $priority = 10;
            $tabs     = json_decode($request_parameters['fields_filter_ids']);

            foreach ($tabs as $index => $group) {
                $groups[] = [
                    'group_widget_id' => $widget_data->id,
                    'widget_data_id'  => $group->widget_data_id,
                    'tab_name'        => $group->tab_name,
                    'by_default'      => $group->default,
                    'priority'        => $priority * ($index + 1)
                ];
            }
        }

        $widget_data->groups()->sync($groups);
    }

    /**
     * @param $widget_data_params
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function saveFromParams(array $widget_data_params)
    {
        $model = $this->model();
        $model->fill($widget_data_params);
        $model->save();

        return $model;
    }


}
