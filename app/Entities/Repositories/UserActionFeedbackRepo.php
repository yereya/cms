<?php


namespace App\Entities\Repositories;


use App\Entities\Models\UserActionFeedback;
use App\Http\Requests\UserActionFeedbackRequest;

/**
 * Class AlertStateRepo
 *
 * @package App\Entities\Repositories
 */
class UserActionFeedbackRepo extends Repository {
	/**
	 * Alert State Model
	 */
	const MODEL = UserActionFeedback::class;


	/**
	 * @param UserActionFeedbackRequest $request
	 *
	 * @return bool
	 */
	public function saveFromRequest( UserActionFeedbackRequest $request ) {
		$model = $this->resolveModel( $request );

		$request            = $request->input();
		$request['user_id'] = auth()->user()->id;

		$model->fill( $request );

		return $model->save();

	}

	public function getResolvedAlertIds() {
		return $this->query()->pluck( 'record_id' );
	}

	private function resolveModel( $request ) {
		$record_id = $request->input( 'record_id' );
		$model     = $this->find( $record_id );

		return count( $model ) ? $model : $this->model();
	}
}