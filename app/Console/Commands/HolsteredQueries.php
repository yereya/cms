<?php namespace App\Console\Commands;

use App\Console\Command;
use App\Libraries\CommandQueueLib;
use App\Libraries\HolsteredQueriesLib;


/**
 * Class SchedulerTasks
 *
 * @package App\Console\Commands
 */
class HolsteredQueries extends Command
{

    /**
     * @var string $signature
     */
    protected $signature = 'query
                            {--id= : Comma delimited list of queries id}
                            {--email_by_permission : Set email sending by account permission } 
                            {--sc : Allows for the script to parse shortcodes}
                            {--email= : Email report}
                            {--sms= : Sms summery}
                            {--alert : Whether to send alerts }
                            {--asCsv : Whether to send it as a Csv attachment}
                            {--email_by_role_id= : Set user role id }';

    /**
     * @var string $description
     */
    protected $description = 'Run holstered queries';

    /**
     * Execute the console command
     */
    public function fire()
    {
        $email               = $this->option('email') ? $this->option('email') : false;
        $sms                 = $this->option('sms') ? $this->option('sms') : false;
        $enable_shortcodes   = $this->option('sc') ? true : false;
        $email_by_permission = $this->option('email_by_permission');
        $email_by_role_id    = $this->option('email_by_role_id');
        $asCsv      = $this->option('asCsv');

        // Create a params array which contains both arguments & options
        $params    = array_merge($this->argument(), $this->option());
        $file_path = CommandQueueLib::start('query', $params);

        $result = HolsteredQueriesLib::fetch($this->option('id'), [
            'email'               => $email,
            'sms'                 => $sms,
            'enable_shortcodes'   => $enable_shortcodes,
            'alert'               => $this->option('alert'),
            'email_by_permission' => $email_by_permission,
            'email_by_role_id'    => $email_by_role_id,
            'asCsv'               => $asCsv
        ]);

        CommandQueueLib::end($file_path);
        var_dump($result);
    }
}