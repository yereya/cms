<?php namespace App\Console;


use App\Libraries\TpLogger;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Illuminate\Console\Command as BaseCommand;

Abstract class Command extends BaseCommand
{
    /**
     * Execute the console command.
     *
     * @param  \Symfony\Component\Console\Input\InputInterface   $input
     * @param  \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return mixed
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            parent::execute($input, $output);
        } catch (\Exception $e) {
            
            // Log All exceptions
            // attempt to set the correct system
            $logger = TpLogger::getInstance();
            if (!$logger->getSystemId()) {
                // If we do not have the system set lets set the default
                $logger->setSystem(6); // 6 is general log system
            }

            $logger->error(['Uncaught Exception -> ' . $e->getMessage()])->error($e);

            
            if (env('APP_DEBUG')) {
                throw $e;
            }
        }
    }


    /**
     * Parse any time string into date
     * uses the strtotime()
     * http://php.net/manual/en/function.strtotime.php
     *
     * @param $str_to_time
     *
     * @return bool|string
     * @throws \Exception
     */
    protected function parseToDate($str_to_time)
    {
        // Clean the extra _ and turn it into spaces
        $str_to_time = str_replace('_', ' ', $str_to_time);

        if (($time = strtotime($str_to_time)) !== false) {
            return date('Y-m-d', $time);
        } else {
            throw new \Exception("Error: the value $str_to_time is not a valid strtotime() parameter.");
        }
    }
}