<?php

namespace App\Http\Controllers\Bo\Accounts;

use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\AccountReportField;
use App\Http\Controllers\Controller;
use App\Http\Traits\AjaxModalTrait;
use Illuminate\Http\Request;

class ReportFieldsController extends Controller
{
    use AjaxModalTrait;

    const PERMISSION = 'bo.advertisers.accounts.report_fields';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form'];

    public function index($account_id)
    {
        return view('pages.bo.accounts.report-fields.index')
            ->with('account_id', $account_id)
            ->with('report_fields', AccountReportField::where('account_id', $account_id)->with('report')->get());
    }

    public function create($account_id)
    {
        return view('pages.bo.accounts.report-fields.form')
            ->with('account', Account::find($account_id))
            ->with('account_reports', $this->getAccountReports($account_id))
            ->with('operators', AccountReportField::$operators)
            ->with('value_types', AccountReportField::$value_types);
    }

    public function store(Request $request, $account_id)
    {
        $AccountReportField = new AccountReportField();
        $request->merge(['account_id' => $account_id]);

        $AccountReportField->fill($request->all());
        $AccountReportField->save();

        return redirect()->route('bo.accounts.{account_id}.report-fields.index', $account_id)
            ->with(self::MESSAGE_TOAST, ["success" => "Account Report Field saved!"]);
    }

    public function edit($account_id, $field_id)
    {
        return view('pages.bo.accounts.report-fields.form')
            ->with('field', AccountReportField::find($field_id))
            ->with('account', Account::find($account_id))
            ->with('account_reports', $this->getAccountReports($account_id))
            ->with('operators', AccountReportField::$operators)
            ->with('value_types', AccountReportField::$value_types);
    }

    public function update(Request $request, $account_id, $field_id)
    {
        $account_report_field = AccountReportField::find($field_id);
        $account_report_field->fill($request->all());
        $account_report_field->save();

        return redirect()->route('bo.accounts.{account_id}.report-fields.index', $account_id)
            ->with(self::MESSAGE_TOAST, ["success" => "Account Report Field updated!"]);
    }

    /**
     * @param $account_id
     * @param $report_field_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($account_id, $report_field_id) {
        if (auth()->user()->can(self::PERMISSION.'.delete')) {
            AccountReportField::destroy($report_field_id);
            return redirect()->route('bo.accounts.{account_id}.report-fields.index', [$account_id])
                ->with('toastr_msgs', ["success" => "Field deleted"]);
        }
        return redirect()->route('bo.accounts.{account_id}.report-fields.index', [$account_id])
            ->with('toastr_msgs', ["warning" => "You do not have permissions to do this."]);
    }

    /**
     * @param $account_id
     *
     * @return array
     */
    private function getAccountReports($account_id)
    {
        $account_reports = [];
        foreach (Account::find($account_id)->reports as $account_report) {
            $account_reports[$account_report->id] = $account_report->report->name;
        }

        return $account_reports;
    }

    /**
     * Ajax Modal Delete Role
     * Delete confirmation
     *
     * @param $params
     * @param $extra_params
     *
     * @return mixed
     */
    private function ajaxModal_deleteField($params, $extra_params)
    {
        return view('pages.delete-confirmation-modal')
            ->with('title', 'Delete report field')
            ->with('btn_title', 'Delete report field')
            ->with('route', ['bo.accounts.{account_id}.report-fields.destroy', $extra_params[0], $extra_params[1]]);
    }
}
