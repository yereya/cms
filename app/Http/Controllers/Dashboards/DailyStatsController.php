<?php namespace App\Http\Controllers\Dashboards;

use App\Http\Controllers\Controller;
use App\Entities\Repositories\Dwh\DashDailyStatsRepo;
use App\Http\Traits\StatsTrait;

/**
 * Class DailyStatsController
 *
 * @package App\Http\Controllers\Dashboard
 */
class DailyStatsController extends Controller
{
    use StatsTrait;

    /**
     * @var string $assets
     */
    protected $assets = ['form'];

    const PERMISSION = 'dashboards.daily-stats';

    /**
     * Index
     *
     * @return string
     */
    public function index()
    {

        $daily_stats_repo = (new DashDailyStatsRepo());

        /*
        /// --- Old ---
        $device         = $daily_stats_repo->getDevices();
        $verticals      = $daily_stats_repo->getVerticals()->pluck('vertical', 'vertical');
        $accounts       = $daily_stats_repo->getAccounts()->pluck('account', 'account_id');
        $advertisers    = $daily_stats_repo->getAdvertisers()->pluck('advertiser_name', 'advertiser_id');
        */

        // --- New ---

        $channel          = array('All Records' => 'All Records', 'Only Display' => 'Only Display', 'Without Display' => 'Without Display');
        $device           = array('u' => 'Unknown','c' => 'Computer','t' => 'Tablet','m' => 'Mobile',);
        $verticals        = $daily_stats_repo->getVerticalsNew();
        $accounts         = $daily_stats_repo->getAccountsNew();
        $advertisers      = $daily_stats_repo->getAdvertisersNew();

        return view('pages.dashboards.daily-stats.index')
            ->with(compact('verticals', 'device', 'accounts', 'advertisers','channel'));
    }
}
