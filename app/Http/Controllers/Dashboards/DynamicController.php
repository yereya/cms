<?php


namespace App\Http\Controllers\Dashboards;


use App\Entities\Repositories\Alerts\DynamicRepo;
use App\Entities\Repositories\Bo\AccountsRepo;
use App\Entities\Repositories\Users\UserRepo;
use App\Http\Controllers\Controller;
use App\Http\Traits\UserActionFeedbackTrait;
use App\Http\Traits\Select2Trait;
use App\Http\Traits\StatsTrait;
use Assets;
use Mockery\Exception;

/**
 * Class DynamicDashboard
 *
 * @package App\Http\Controllers\Dashboards
 */
class DynamicController extends Controller
{
    use StatsTrait;
    use Select2Trait;
    use UserActionFeedbackTrait;

    /**
     * @var string $assets
     */
    protected $assets = ['form', 'top-app'];

    /**
     *  Permission name
     */
    const PERMISSION = 'dashboards.dynamic';


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        Assets::addCss("assets/global/plugins/nestable2/jquery.nestable.css");

        $model_repo  = (new DynamicRepo());
        $user        = (new UserRepo())->find(auth()->user()->id);
        $resolved    = $model_repo->getResolved();
        $accounts    = (new AccountsRepo())->getActiveUserAccounts();
        $advertisers = $model_repo->getAdvertisers();


        return view('pages.dashboards.dynamic.index')
            ->with(compact('resolved', 'user', 'accounts', 'advertisers'));
    }

    /**
     * Get accounts for  level filter
     *
     * @param $params
     *
     * @return mixed
     */
    public function select2_getAccounts($params)
    {
        $advertiser_id = $params['dependency'][0]['value'];

        //set date_range value for date dependency
        request()->request->add(['date_range' => ['value' => $params['param'][0]['value']]]);

        $result = (new DynamicRepo())->getAccountsByAdvertiserId($advertiser_id);

        if (count($result)) {

            //Transform the fields to select2 fields
            $result->transform(function ($value) {
                return [
                    'id'   => $value->account_id,
                    'text' => $value->account_name
                ];
            });
        }

        return $result;
    }

    /**
     * Get campaigns for level filter
     *
     * @param $params
     *
     * @return \Illuminate\Support\Collection
     */
    public function select2_getCampaigns($params)
    {
        try {
            $advertiser_id = $params['dependency'][0]['value'];
            $account_id    = $params['dependency'][1]['value'];

        } catch (Exception $exception) {
            return 'one of the fields did\'nt set';
        }

        //Set date_range value for date dependency
        request()->request->add(['date_range' => ['value' => $params['param'][0]['value']]]);
        $result = (new DynamicRepo())->getCampaignsByAccountId($advertiser_id, $account_id);

        if (count($result)) {
            //Transform the fields to select2 fields
            $result->transform(function ($value) {
                return [
                    'id'   => $value->campaign_id,
                    'text' => $value->campaign_name
                ];
            });
        }

        return $result;
    }
}