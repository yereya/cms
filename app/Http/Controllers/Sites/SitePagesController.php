<?php

namespace App\Http\Controllers\Sites;

use App\Entities\Models\Sites\ContentTypes\SiteContentType;
use App\Entities\Models\Sites\Page;
use App\Entities\Models\Sites\PageContentType;
use App\Entities\Models\Sites\Post;
use App\Entities\Models\Sites\Site;
use App\Entities\Repositories\Sites\LabelRepo;
use App\Entities\Repositories\Sites\PageRepo;
use App\Entities\Repositories\Sites\PostRepo;
use App\Entities\Repositories\Sites\TemplateRepo;
use App\Facades\SiteConnectionLib;
use App\Http\Controllers\Controller;
use App\Http\Requests\Sites\SitePagesFormRequest;
use App\Http\Traits\AjaxAlertTrait;
use App\Http\Traits\AjaxModalTrait;
use App\Http\Traits\ChoiceTrait;
use App\Http\Traits\Select2Trait;
use App\Libraries\PageInfo\PageTemplatesPostsInfoLib;
use Illuminate\Http\Request;

/**
 * Class SitePagesController
 *
 * @package App\Http\Controllers\Sites
 */
class SitePagesController extends Controller
{
    use AjaxModalTrait;
    use ChoiceTrait;
    use AjaxAlertTrait;
    use Select2Trait;

    const PERMISSION = 'top.pages';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form', 'top-app', 'nestable'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Site Pages Management';

    private $site;

    /**
     * SitePagesController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->site = SiteConnectionLib::getSite();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('pages.site.pages.index')
            ->withSite($this->site)
            ->withCan($this->buildCanPermissions())
            ->withPagesTree((new PostRepo())->getPagesAsTree());
    }

    /**
     * Create
     *
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('pages.site.pages.form')
            ->withSite($this->site)
            ->withPageTypes(Page::pageTypes)
            ->withSlug((new PageRepo())->buildHref($this->site->id, $request->get('parent_id')))
            ->withParentId($request->get('parent_id'));
    }

    /**
     * Store
     *
     * @param SitePagesFormRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SitePagesFormRequest $request)
    {
        $values = $this->insertPriority($request->all());

        $message = (new PostRepo())->savePage($values, new Page(), new Post());

        if (isset($message['error'])) {
            return redirect()->back()->with(self::MESSAGE_TOAST, $message);
        }

        return $request->get('create_another')
            ? redirect()->back()->with(self::MESSAGE_TOAST, $message)
            : redirect()->route('sites.{site}.pages.index', $this->site->id)->with(self::MESSAGE_TOAST, $message);
    }

    /**
     * Insert Page priority
     *
     * @param $values
     *
     * @return mixed
     */
    private function insertPriority($values)
    {
        if (isset($values['parent_id']) && $values['parent_id'] > 0) {
            $priority = Post::where('parent_id', $values['parent_id'])->max('priority');
        } else {
            $priority            = Post::whereNull('parent_id')->max('priority');
            $values['parent_id'] = null;
        }

        $values['priority'] = $priority ? $priority : 1;

        return $values;
    }

    /**
     * Edit
     *
     * @param Site $site
     * @param Post $post
     *
     * @return \Illuminate\View\View
     * @internal param Page $page
     */
    public function edit(Site $site, Post $post)
    {
        return view('pages.site.pages.form')
            ->withSite($site)
            ->withPageTypes(Page::pageTypes)
            ->withPost($post)
            ->withSlug((new PageRepo())->buildHref($site->id, $post->parent_id))
            ->withParentId($post->parent_id);
    }

    /**
     * Update
     *
     * @param SiteDomainsFormRequest|SitePagesFormRequest $request
     * @param Site $site
     * @param Page|Post|int $post
     *
     * @return \Illuminate\Http\Response
     */
    public function update(SitePagesFormRequest $request, Site $site, Post $post)
    {
        $message = (new PostRepo())->savePage($request->all(), $post->page ?? new Page(), $post);

        if (isset($message['error'])) {
            return redirect()->back()->with(self::MESSAGE_TOAST, $message);
        }

        return $request->get('create_another')
            ? redirect()->back()->with(self::MESSAGE_TOAST, $message)
            : redirect()->route('sites.{site}.pages.index', $site->id)->with(self::MESSAGE_TOAST, $message);
    }

    /**
     * Delete page with all is sub pages
     *
     * @param Request $request
     * @param $site_id
     * @param $post_id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $site_id, $post_id)
    {
        if (!auth()->user()->can(self::PERMISSION . '.delete')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        if ($request->filters) {

            return $this->destroyFromPostPage($request, $site_id, $post_id);
        } else {

            return $this->destroyFromPagesPage($site_id, $post_id);
        }

    }

    /**
     * Destroy From Posts Page
     *
     * @param Request $request
     * @param $site_id
     * @param $post_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroyFromPostPage(Request $request, $site_id, $post_id)
    {

        $trashed = !!$request->trashed;

        if($trashed){
            $page = Post::onlyTrashed()->find($post_id);
            $page->forceDelete();
        } else {
            $post_repo = new PostRepo();
            $page = $post_repo->deletePage($post_id);
        }

        $filters = json_decode($request->filters, true);
        $route_params = array_merge([$site_id], $filters);

        return redirect()->route('sites.{site}.posts.index', $route_params)
            ->with(self::MESSAGE_TOAST, ["success" => "Page {$page->name} deleted"]);

    }

    /**
     * Destroy From Pages Page
     *
     * @param         $site_id
     * @param         $post_id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroyFromPagesPage($site_id, $post_id)
    {

        $post_repo = new PostRepo();
        $page      = $post_repo->deletePage($post_id);


        return redirect()->route('sites.{site}.pages.index', $site_id)
            ->with(self::MESSAGE_TOAST, ["success" => "Page {$page->name} deleted"]);
    }

    /**
     * Send alert with list of pages need to be deleted
     * This Alert can be called from two places from "Pages" page and from "Posts" page
     * and it return back to the page where it came from.
     *
     * @return mixed
     */
    public function ajaxAlert_delete()
    {
        $request = request();
        $post_id = $request->route()->getParameter('page');
        $trashed = !!$request->trashed;

        if($trashed) {
            $text = 'Do you want to permanently delete this post?';
        } else {
            $post           = Post::where('id', $post_id)->first();
            $pages_list     = (new PostRepo())->getPagesAsTree($post->id)->flatten();
            $sub_pages_list = $pages_list->map(function ($item) {
                return $item->name;
            });

            $text = view('pages.site.ajax-custom-alert')
                ->with('name', $post->name)
                ->with('sub_list', $sub_pages_list)
                ->render();
        }

        return view('partials.containers.alerts.ajax-alert')
            ->with('text', $text)
            ->with('route', 'sites.{site}.pages.destroy')
            ->with('action', 'delete')
            ->with('route_params', [$this->site->id, $post_id, 'trashed' => $trashed, 'filters' => $request->get('filters')]);
    }

    /**
     * Create Page block by type - archive|static|redirect
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function choice_pageType(Request $request)
    {
        if ($request->has('type') && is_null($request->get('type'))) {
            return response("type: not found", 404);
        }

        $html = view()->make('partials.containers.sites.page.repeater');

        $html->withType($request->get('type'));
        $html->withTemplates(TemplateRepo::getAllInSelectFormat());
        $html->withPost(Post::find($request->get('page')));

        if ($request->get('type') == 'redirect') {
            $html->withPages((new PostRepo())->getAllInSelectFormat());
        }

        if ($request->get('type') == 'archive') {
            $html->withContentTypes(SiteContentType::where('site_id', $this->site->id)->get());
            $html->withLabels(LabelRepo::getAllInSelectFormat());
        }

        if ($request->get('type') == 'archive' && $request->has('page')) {
            $html->withTemplatesArchive(PageContentType::where('post_id', $request->get('page'))->get());
            $html->withLabels(LabelRepo::getAllInSelectFormat());
        };

        return $html->render();
    }

    /**
     * Return pages list to select2
     *
     * @param $params
     *
     * @return mixed
     */
    public function select2_pagesList($params)
    {
        // Pull preset value
        if (isset($params['getCurrentValue'])) {
            //TODO
        } elseif (isset($params['query']) && count($params['query']) > 0) {
            return Page::select('id', 'name as text')->where('name', 'LIKE', '%' . $params['query'] . '%')->get();
        } else {
            $pages = (new PageRepo())->pageListToSelect();
            return $pages->toArray();
        }
    }

    /**
     * pagesListWithPaths
     *
     * Return array pages with paths
     *
     * @return array
     */
    public function pagesListWithPaths()
    {
        return (new PageRepo())->pageListWithPaths();
    }

    /**
     * Store Pages Orders
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storePagesOrder(Request $request) {
        if ($request->has('list')) {
            $list = json_decode($request->get('list'));
            $post_repo = new PostRepo();
            $pages = $post_repo->getAllPages();

            foreach ($pages as $page) {
                if (isset($list->{$page->id})) {
                    $new_page_info = $list->{$page->id};
                    $page->parent_id = empty($new_page_info->parent_id) ? null : $new_page_info->parent_id;
                    $page->priority = $new_page_info->priority;
                }
            }

            $validation_result = $this->validateSlug($pages);
            if ($validation_result === true) {
                foreach ($pages as $page) {
                    $page->save();
                }
            }
        }

        return redirect()->route('sites.{site}.pages.index', $this->site->id);
    }

    /**
     * Validate the no 2 pages has the same slug and parent_id
     *
     * @param $pages
     *
     * @return array
     */
    private function validateSlug($pages)
    {
        $slug_parent_id_validation_map_helper = [];
        foreach ($pages as $page) {
            $key = $page->parent_id . $page->slug;
            if (!isset($slug_parent_id_validation_map_helper[$key])) {
                $slug_parent_id_validation_map_helper[$key] = $page->name;
            } else {
                return
                    "Error: Pages \"" . $page->name . '" and "' . $slug_parent_id_validation_map_helper[$key] . '" have the same slug under the same parent';
            }
        }
        return true;
    }

    /**
     * Ajax Modal Templates Info
     */
    private function ajaxModal_templatesInfo()
    {
        $site = SiteConnectionLib::getSite();
        $page_id = request()->route()->getParameter('page');
        $page = Page::where('post_id', $page_id)->first();
        $page_info_lib = new PageTemplatesPostsInfoLib($site);
        $page_info_lib->build($page);
        $posts_info = $page_info_lib->getPostsInfo();
        $templates_info = $page_info_lib->getTemplatesInfo();

        return view('pages.site.pages.templates-info')
            ->with(compact('templates_info', 'posts_info'));
    }
}
