<?php

namespace App\Http\Controllers\Sites;

use App\Entities\Models\Sites\Post;
use App\Entities\Models\Sites\Ribbon;
use App\Entities\Models\Sites\Site;
use App\Entities\Repositories\Sites\PostRepo;
use App\Entities\Repositories\Sites\PostRibbonRepo;
use App\Entities\Repositories\Sites\RibbonRepo;
use App\Entities\Repositories\Sites\Widgets\WidgetDataRepo;
use App\Http\Controllers\Controller;
use App\Http\Requests\Sites\RibbonsFormRequest;
use App\Http\Traits\AjaxAlertTrait;
use App\Libraries\Widgets\Helpers\DynamicList;
use App\Libraries\Widgets\Traits\DynamicListTrait;
use Illuminate\Foundation\Application;
use App\Facades\SiteConnectionLib;
use Illuminate\Http\Request;
use Route;

class SiteRibbonsController extends Controller
{
    use AjaxAlertTrait;
    use DynamicListTrait;

    /**
     * Permission name
     */
    const PERMISSION = 'top.ribbon';

    /**
     * @var array
     */
    protected $assets = ['datatables', 'form', 'top-app'];

    /**
     * @var site
     */
    private $site;

    /**
     * @var Ribbon repo
     */
    private $repo;

    /**
     * SiteRibbonsController constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->site = SiteConnectionLib::getSite();
        $this->repo = new RibbonRepo();
        parent::__construct($app);
    }

    /**
     * Index
     *
     * @return mixed
     */
    public function index()
    {
        return $this->shareCommonParams(view('pages.site.ribbons.index')
            ->with('ribbons', $this->repo->getAll()));
    }

    /**
     * Create
     *
     * @return mixed
     */
    public function create()
    {
        return $this->shareCommonParams(view('pages.site.ribbons.form'));
    }

    /**
     * Store
     *
     * @param RibbonsFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RibbonsFormRequest $request)
    {
        try {
            $this->repo->fill($request->all());

            return $this->redirectHelper('Sites\SiteRibbonsController@index', 'success', 'Ribbon saved successful');
        } catch (\Exception $e) {
            return $this->redirectHelper('Sites\SiteRibbonsController@create', 'warning', 'The Ribbon don\'t saved');
        }
    }

    /**
     * Edit
     *
     * @param Site $site
     * @param Ribbon $ribbons
     *
     * @return mixed
     */
    public function edit(Site $site, Ribbon $ribbons)
    {
        return $this->shareCommonParams(view('pages.site.ribbons.form')->with('ribbon', $ribbons));
    }

    /**
     * Update
     *
     * @param RibbonsFormRequest $request
     *
     * @param Site $site
     * @param Ribbon $ribbons
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(RibbonsFormRequest $request, Site $site, Ribbon $ribbons)
    {
        try {
            $this->repo->updateRibbon($ribbons, $request->all());

            return $this->redirectHelper('Sites\SiteRibbonsController@index', 'success', 'Ribbon updated successful');
        } catch (\Exception $e) {
            return $this->redirectHelper('Sites\SiteRibbonsController@edit', 'warning', 'The Ribbon don\'t updated', $ribbons);
        }
    }

    /**
     * Destroy
     *
     * @param Site $site
     * @param Ribbon $ribbons
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Site $site, Ribbon $ribbons)
    {
        try {
            $this->repo->delete($ribbons);

            return $this->redirectHelper('Sites\SiteRibbonsController@index', 'success', 'Ribbon deleted successful');
        } catch (\Exception $e) {
            return $this->redirectHelper('Sites\SiteRibbonsController@index', 'warning', 'The Ribbon don\'t deleted');
        }
    }

    /**
     * Ajax Alert Delete ribbon
     *
     * @return $this
     */
    public function ajaxAlert_deleteRibbon()
    {
        $ribbon_id = Route::current()->getParameter('ribbons');

        return view('partials.containers.alerts.ajax-alert')
            ->with('text', 'Please confirm deleting ribbon #' . $ribbon_id)
            ->with('route', 'sites.{site}.ribbons.destroy')
            ->with('action', 'delete')
            ->with('route_params', [$this->site->id, $ribbon_id]);
    }

    /**
     * Return to dynamic list
     * post list by content type
     * ribbon list
     *
     * @param Request $request
     *
     * @return $this|null
     */
    public function ribbonPost(Request $request)
    {
        if (!$request->has('content_type_id')) {
            return null;
        }

        $widget_dynamic = (new WidgetDataRepo)->getDynamicList($request->get('widget_data_id'));
        $widget_dynamic_list = new DynamicList($this->buildDynamicListParams($widget_dynamic));
        $posts = $widget_dynamic_list->getBasePosts();

        return view()->make('widgets-forms.dynamic-list.ribbons-list')
            ->with('posts', $posts->pluck('name', 'id'))
            ->with('ribbons', $this->repo->getAll())
            ->with('post_ribbons', (new PostRibbonRepo())->postRibbon($request->get('widget_data_id')));
    }

    /**
     * Share common params
     * site
     * permission
     *
     * @param $view
     *
     * @return mixed
     */
    private function shareCommonParams($view)
    {
        return $view
            ->with('site', $this->site)
            ->with('can', $this->buildCanPermissions());
    }

    /**
     * Redirect helper
     *
     * @param $action
     * @param $status
     * @param $message
     * @param Ribbon $ribbon
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    private function redirectHelper($action, $status, $message, Ribbon $ribbon = null)
    {
        $values['site'] = $this->site->id;
        if ($ribbon) {
            $values['ribbons'] = $ribbon->id;
        }

        return redirect()->action($action, $values)->with(self::MESSAGE_TOAST, [$status => $message]);
    }
}