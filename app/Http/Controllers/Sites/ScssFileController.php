<?php

namespace App\Http\Controllers\Sites;

use App\Entities\Repositories\ScssFileRepo;
use Illuminate\View\View;
use App\Http\Controllers\Controller;
use App\Entities\Models\Sites\Site;
use App\Http\Requests\StoreScssFileRequest;
use App\Http\Requests\UpdateScssFileRequest;

class ScssFileController extends Controller
{
    /**
     * List of assets to load.
     *
     * @ver array
     */
    protected $assets = ['code-mirror', 'jquery-ui'];

    /**
     * Controller permission name.
     *
     * @ver string
     */
    const PERMISSION = 'top.settings.scss-files';

    /**
     * Render the sass editor view.
     *
     * @param Site $site
     *
     * @return View
     */
    public function index(Site $site)
    {
        $scss_files = (new Site())->getScssFilesById($site->id);

        return view('pages.site.settings.scss-files.edit')
            ->with(compact('site', 'scss_files'));
    }

    /**
     * Store a new sass file.
     *
     * @param Site $site
     * @param int  $id
     *
     * @return mixed
     */
    public function show(Site $site, $id)
    {
        $scss_file  = null;
        $scss_files = (new Site())->getScssFilesById($site->id);

        foreach ($scss_files as $file) {
            if ($file->id == $id) {
                $scss_file = $file;
                break;
            }
        }

        if (!$scss_file) {
            return redirect()
                ->route('sites.{site}.settings.scss-files.index', $site->id)
                ->with('toastr_msgs', ['File not found.']);
        }

        $url = route('sites.{site}.settings.scss-files.update', [$site->id, $scss_file->id]);

        return view('pages.site.settings.scss-files.edit')
            ->with(compact('site', 'scss_file', 'url', 'scss_files'));
    }

    /**
     * Store a new sass file.
     *
     * @param Site                 $site
     * @param StoreScssFileRequest $request
     *
     * @return mixed
     */
    public function store(Site $site, StoreScssFileRequest $request)
    {
        $file = $site->scssFiles()->create($request->all());

        if (!$file) {

            return redirect()->back()
                ->withInput()
                ->with('error', 'SASS file was not saved.');
        }

        return redirect()->route('sites.{site}.settings.scss-files.show', [$site->id, $file->id])
            ->with('success', 'File was successfully saved.');
    }

    /**
     * Update a sass file resource.
     *
     * @param Site                  $site
     * @param int                   $id
     * @param UpdateScssFileRequest $request
     *
     * @return mixed
     */
    public function update(Site $site, $id, UpdateScssFileRequest $request)
    {
        $file = $site->scssFiles()->find($id);

        if (!$file || !$file->update($request->all())) {

            return redirect()->back()
                ->withInput()
                ->with('error', 'SASS file was not saved.');
        }

        return redirect()->back()
            ->with('success', 'File was successfully saved.');
    }

    /**
     * Delete a sass file resource.
     *
     * @param Site $site
     * @param int  $file_id
     *
     * @return mixed
     */
    public function destroy(Site $site, $file_id)
    {
        $file = $site->scssFiles()->find($file_id);

        if (!$file || !$file->delete()) {

            return redirect()->back()
                ->withInput()
                ->with('error', 'SASS file was not deleted.');
        }

        return redirect()->route('sites.{site}.settings.scss-files.index', [$site->id])
            ->with('success', 'File was successfully deleted.!');
    }

    /**
     * Update Scss Files Order
     */
    public function updatePriorities()
    {
        $response = [
            'desc'   => 'Some thing went wrong for more info check with the admin',
            'status' => 'fail'
        ];

        $model_repo = new ScssFileRepo();
        $ids        = request()->input('ids');
        $updated    = $model_repo->updatePriorities($ids);
        if ($updated) {
            $response = [
                'desc'   => 'Files order updated',
                'status' => 'success'
            ];
        }

        return $response;
    }

}
