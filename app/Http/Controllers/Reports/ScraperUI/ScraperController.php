<?php

namespace App\Http\Controllers\Reports\ScraperUI;

use App\Entities\Models\Log;
use App\Entities\Models\Reports\Publishers\BrandScraper\Scraper;
use App\Entities\Models\Reports\Publishers\BrandScraper\ScraperAdvanceProperties;
use App\Entities\Models\Reports\Publishers\BrandScraper\ScraperFetcher;
use App\Entities\Models\Reports\Publishers\BrandScraper\ScraperFetcherProperties;
use App\Entities\Models\Reports\Publishers\BrandScraper\ScraperProcessor;
use App\Entities\Models\Scheduler\Task;
use App\Entities\Models\Users\User;
use App\Entities\Repositories\Reports\Publishers\BrandPerformance\ScraperRepo;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\Reports\BrandScraper\FormBrandScraperRequest;
use App\Libraries\Scheduler\SchedulerTasksLib;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Mockery\CountValidator\Exception;

class ScraperController extends Controller
{
    const PERMISSION = 'scraper';

    /**
     * @var string $assets
     */
    protected $assets = ['form', 'datatables', 'scraper'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Scraper';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.bo.scraper-brand-performance.index')
            ->with('scrapers', Scraper::with('advertisers')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $day_hours = ['00:00','01:00','02:00','03:00','04:00','05:00','06:00','07:00','08:00','09:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00'];
        $time_between_executions = [15 => '15 Minutes', 30 => '30 Minutes', 60 => '1 Hour', 120 => '2 Hours', 180 => '3 Hours', 360 => '6 Hours', 720 => '12 hours', 1440 => '1 Day'];

        return view('pages.bo.scraper-brand-performance.form')
            ->with('time_between_executions', $time_between_executions)
            ->with('day_hours', $day_hours);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param FormBrandScraperRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(FormBrandScraperRequest $request)
    {
        $params           = $request->all();
        $scraper          = Scraper::create($params);
        $params['active'] = isset($params['active']) && $params['active'] ? 1 : 0;

        if (isset($scraper->id)) {
            // Save to scheduler tasks
            $task_params = [
                'run_time_start' => $params['run_time_start'],
                'run_time_stop'  => $params['run_time_stop'],
                'repeat_delay'   => $params['repeat_delay'],
                'task_name'      => $params['scraper_name'],
                'active'         => $params['active']
            ];
            SchedulerTasksLib::updateOrCreate(1, $scraper->id, $task_params);
        }

        if ($request->get('create_another')) {
            return redirect()->route('reports.scraper.create')
                ->with(self::MESSAGE_TOAST, ["success" => "New scraper added (ID: $scraper->id)"]);
        }

        return redirect()->route('reports.scraper.index')
            ->with(self::MESSAGE_TOAST, ["success" => "New scraper added (ID: $scraper->id)"]);
    }

    /**
     * @param $scraper_id
     *
     * @return mixed
     * @throws \Exception
     */
    public function show($scraper_id)
    {
        $scraper = Scraper::with('advertisers')->find($scraper_id);

        if (!$scraper) {
            throw new \Exception('the scraper you have provided does not exists');
        }

        return view('pages.bo.scraper-brand-performance.form')
            ->with('scraper', $scraper);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $scraper_id
     *
     * @return $this
     * @throws \Exception
     */
    public function edit($scraper_id)
    {
        $scraper = Scraper::with('advertisers')->find($scraper_id);

        if (!$scraper) {
            throw new \Exception('the scraper you have provided does not exists');
        }

        $day_hours               = [
            '00:00',
            '01:00',
            '02:00',
            '03:00',
            '04:00',
            '05:00',
            '06:00',
            '07:00',
            '08:00',
            '09:00',
            '11:00',
            '12:00',
            '13:00',
            '14:00',
            '15:00',
            '16:00',
            '17:00',
            '18:00',
            '19:00',
            '20:00',
            '21:00',
            '22:00',
            '23:00'
        ];
        $time_between_executions = [15   => '15 Minutes',
                                    30   => '30 Minutes',
                                    60   => '1 Hour',
                                    120  => '2 Hours',
                                    180  => '3 Hours',
                                    240  => '4 Hours',
                                    360  => '6 Hours',
                                    720  => '12 hours',
                                    1440 => '1 Day'
        ];

        $scraper['properties'] = ScraperAdvanceProperties::where('scraper_id', $scraper_id)->get();
        $fetchers              = ScraperFetcher::where('scraper_id', $scraper_id)->orderBy('priority', 'ASC')->get();

        $fetchers_results = [];
        foreach ($fetchers as $fetcher) {
            $fetcher['header']       = ScraperFetcherProperties::where('fetcher_id', $fetcher['id'])
                ->where('type_field', ScraperFetcher::SCRAPER_PROPERTIES_TYPE_HEADER)
                ->get();
            $fetcher['body']         = ScraperFetcherProperties::where('fetcher_id', $fetcher['id'])
                ->where('type_field', ScraperFetcher::SCRAPER_PROPERTIES_TYPE_FORM_PARAMS)
                ->get();
            $fetcher['translators']  = ScraperFetcherProperties::where('fetcher_id', $fetcher['id'])
                ->where('type_field', ScraperFetcher::SCRAPER_PROPERTIES_TYPE_TRANSLATOR)
                ->get();
            $fetcher['queries']      = ScraperFetcherProperties::where('fetcher_id', $fetcher['id'])
                ->where('type_field', ScraperFetcher::SCRAPER_PROPERTIES_TYPE_QUERY)
                ->get();
            $fetcher['spreadsheet_info'] = ScraperFetcherProperties::where('fetcher_id', $fetcher['id'])
                ->where('type_field', ScraperFetcher::SCRAPER_PROPERTIES_TYPE_SPREADSHEET_INFO)
                ->get();
            $fetchers_results[]      = $fetcher;
        }

        $processors = ScraperProcessor::where('scraper_id', $scraper_id)->get()->toArray();

        $translators = $this->getTranslators();

        return view('pages.bo.scraper-brand-performance.form')
            ->with('scraper', $scraper)
            ->with('day_hours', $day_hours)
            ->with('time_between_executions', $time_between_executions)
            ->with('fetchers', $fetchers_results)
            ->with('processors', $processors)
            ->with('translators', $translators['translators'])
            ->with('translators_documentations', $translators['params'])
            ->with('users', User::all()->keyBy('id'))
            ->with('logs', Log::whereNull('parent_id')->where('task_id', $scraper_id)->where('system_id', 2)->get());
    }


    public function mail(Request $request)
    {
        // TODO email 
        $request = $request->all();



        dd($request);
    }


    /**
     * Get Translators
     *
     * @return array
     */
    private function getTranslators()
    {
        $translators = new ScraperRepo();
        $translators = $translators->getAvailableTranslators();
        $results     = [];

        foreach ($translators as $translator) {
            $name      = $translator['class'] . '@' . $translator['method'];
            $results['translators'][] = ['id' => $name, 'text' => $name];
            $results['params'][$name] = $translator['params'];
        }

        return $results;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param FormBrandScraperRequest $request
     * @param $scraper_id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(FormBrandScraperRequest $request, $scraper_id)
    {
        $params = $request->all();

        // Validate params
        $params['active']           = isset($params['active']) && $params['active'] ? 1 : 0;
        $params['save_to_adserver'] = isset($params['save_to_adserver']) && $params['save_to_adserver'] ? 1 : 0;
        $params['run_time_start']   = $params['run_time_start'] != "0:00" ? $params['run_time_start'] : null;
        $params['run_time_stop']    = $params['run_time_stop'] != "0:00" ? $params['run_time_stop'] : null;

        Scraper::find($scraper_id)->fill($params)->save();

        // Save to scheduler tasks
        $task_params = [
            'run_time_start' => $params['run_time_start'],
            'run_time_stop'  => $params['run_time_stop'],
            'repeat_delay'   => $params['repeat_delay'],
            'task_name'      => $params['scraper_name'],
            'active'         => $params['active']
        ];
        SchedulerTasksLib::updateOrCreate(1, $scraper_id, $task_params);

        if ($request->ajax()) {
            $this->advancedUpdate($params['properties'], $scraper_id);
            return [
                'status'            => 1,
                self::MESSAGE_TOAST => 'Saved'
            ];
        }

        return redirect()->route('reports.scraper.index')
            ->with(self::MESSAGE_TOAST, ["success" => "Scraper (ID: $scraper_id) updated!"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $scraper_id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($scraper_id)
    {
        Scraper::find($scraper_id)->delete();
        return ['status' => 1];
    }

    /**
     * Duplicate current scraper
     *
     * @param $scraper
     * @return \Illuminate\Http\RedirectResponse
     */
    public function duplicate($scraper) {
        $scr = Scraper::with(['fetchers', 'advancedProperties', 'processors'])->find($scraper);
        $cloneScraper = $scr->replicate();
        $cloneScraper->push();
        try {
            foreach ($scr->advancedProperties as $property) {
                $cloneProperty = $property->replicate();
                $cloneProperty->scraper_id = $cloneScraper->id;
                $cloneProperty->push();
            }

            foreach ($scr->fetchers as $fetcher) {
                $cloneFetcher = $fetcher->replicate();
                $cloneFetcher->scraper_id = $cloneScraper->id;
                $cloneFetcher->push();

                foreach ($fetcher->fetcherProperties as $property) {
                    $cloneProperty = $property->replicate();
                    $cloneProperty->fetcher_id = $cloneFetcher->id;
                    $cloneProperty->push();
                }
            }

            foreach ($scr->processors as $processor) {
                $cloneProcessor = $processor->replicate();
                $cloneProcessor->scraper_id = $cloneScraper->id;
                $cloneProcessor->push();
            }

            return redirect()->route('reports.scraper.edit', $cloneScraper->id)
                ->with(self::MESSAGE_TOAST, ["success" => "Scraper duplicate (ID: $cloneScraper->id) successful!"]);
        } catch (\Exception $e) {
            return redirect()->route('reports.scraper.index')
                ->with(self::MESSAGE_TOAST, ["error" => "Scraper duplicate ERROR! " . $e->getMessage()]);
        }
    }

    /**
     * save advanced params
     *
     * @param $advanced
     * @param $scraper_id
     *
     * @return bool
     */
    private function advancedUpdate($advanced, $scraper_id)
    {
        ScraperAdvanceProperties::where('scraper_id', $scraper_id)->delete();

        $properties = clearAttributesArray($advanced['key'], $advanced['value']);
        try {
            $advanced_proper = [];
            foreach ($properties as $key => $value) {
                $advanced_proper[] = [
                    'scraper_id' => $scraper_id,
                    'key'        => $key,
                    'value'      => $value,
                    'type_field' => ScraperFetcher::SCRAPER_PROPERTIES_TYPE_FORM_PARAMS
                ];
            }

            ScraperAdvanceProperties::insert($advanced_proper);
        } catch (Exception $e) {
            return false;
        }

        return true;
    }
}
