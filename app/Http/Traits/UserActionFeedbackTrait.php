<?php


namespace App\Http\Traits;


use App\Entities\Repositories\UserActionFeedbackRepo;
use App\Http\Requests\UserActionFeedbackRequest;

trait UserActionFeedbackTrait
{
    /**
     * save alert states
     *
     * @param UserActionFeedbackRequest $request
     *
     * @return array
     */
    public function saveAlertStates(UserActionFeedbackRequest $request)
    {
        $alert_state_repo    = (new UserActionFeedbackRepo());
	    $multi_records_state = $request->input( 'records_id' );

	    if ( $multi_records_state ) {
		    $record_ids = collect( explode( ',', $request->input( 'records_id' ) ) );
		    $saved      = $record_ids->each( function ( $id ) use ( $request, $alert_state_repo ) {
			    $request['record_id'] = $id;

			    return $alert_state_repo->saveFromRequest( $request );
		    } );
	    } else {
		    $saved = $alert_state_repo->saveFromRequest( $request );
	    }

        return $saved
            ? ['status' => 200]
            : ['status' => 500];
    }
}