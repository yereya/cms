<?php

namespace App\Http\Requests\Bo\Publishers;

use App\Http\Requests\Request;

class PublishersRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->getMethod()) {
            case 'POST':
                return [
                    'publisher_name' => 'required|unique:bo.publishers,publisher_name',
                    'status' => 'required',
                    'type' => 'required'
                ];
            case 'PUT':
                return [
                    'publisher_name' => 'required|unique:bo.publishers,publisher_name,' . $this->route()->getParameter('publishers'),
                    'status' => 'required',
                    'type' => 'required'
                ];
        }
    }
}
