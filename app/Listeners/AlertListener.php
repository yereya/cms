<?php namespace App\Listeners;

use App\Events\AlertEvent;
use App\Libraries\Alerts\AlertTableAccountTransformer;
use App\Libraries\Alerts\AlertTableBudgetTransformer;
use App\Libraries\Alerts\AlertTableCampaignTransformer;
use App\Libraries\Alerts\AlertTableSystemErrorTransformer;
use App\Libraries\Alerts\AlertWeeklyAccountClickPerformance;
use App\Libraries\Alerts\AlertWeeklyAdGroupClickPerformance;
use App\Libraries\Alerts\AlertWeeklyCampaignClickPerformance;

/**
 * Class AlertListener
 * @package App\Listeners
 */
class AlertListener
{
    /**
     * Handle the event.
     *
     * @param  AlertEvent $event
     * @return void
     */
    public function handle(AlertEvent $event)
    {
        $now = sqlDateFormat('now');
        $table = '';
        $data = [];
        switch ($event->query_id) {
            case 27 :
                $data = AlertTableBudgetTransformer::transform($event->result, $now);
                $table = 'budget';
                break;
            case 2 :
                $data = AlertTableSystemErrorTransformer::transform($event->result, $now);
                $table = 'system_errors';
                break;
            case 4:
                $data = AlertTableCampaignTransformer::transform($event->result, $now);
                $table = 'campaign';
                break;
            case 26:
                $data = AlertTableAccountTransformer::transform($event->result, $now);
                $table = 'account';
                break;
            case 127:
                $data = AlertWeeklyCampaignClickPerformance::transform($event->result, $now);
                $table = 'weekly_campaign_click_performance';
                break;
            case 128:
                $data = AlertWeeklyAccountClickPerformance::transform($event->result, $now);
                $table = 'weekly_account_click_performance';
                break;
            case 129:
                $data = AlertWeeklyAdGroupClickPerformance::transform($event->result, $now);
                $table = 'weekly_adgroup_click_performance';
                break;
        }

        // upsert - batch insert the array or update on duplicate
        insertOnDuplicateKeyUpdate(\DB::connection('alert')->table($table), $data);
    }
}
