<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class LibrariesServiceProvider
 *
 * @package App\Providers
 */
class LibrariesServiceProvider extends ServiceProvider
{

    /**
     * @var string $namespace
     */
    protected $namespace = 'App\Libraries';

    /**
     * key is the facade name, value is the namespace starting from library
     * @var array $libraries
     */
    protected $libraries = [
        'Layout' => 'Layout',
        'ShortCode' => 'ShortCodes\\ShortCodesLib'
    ];

    /**
     * Registers the helpers
     */
    public function register()
    {
        foreach ($this->libraries as $key => $lib) {
            $this->app->bind(strtolower($key), $this->namespace . '\\' . $lib);
        }
    }
}
