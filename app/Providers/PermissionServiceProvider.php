<?php

namespace App\Providers;

use App\Entities\Models\Users\AccessControl\Permission;
use App\Entities\Models\Users\User;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;

/**
 * Class AuthServiceProvider
 *
 * @package App\Providers
 */
class PermissionServiceProvider extends ServiceProvider
{

    /**
     * @var array $types
     */
    protected $types = [
        'view',
        'add',
        'edit',
        'delete'
    ];

    /**
     * Register
     */
    public function register()
    {
        //
    }

    /**
     * Boot
     *
     * @param GateContract $gate
     */
    public function boot(GateContract $gate)
    {
        /*  --- Old ---

         $this->getPermissions()->map(function (Permission $permission) use ($gate) {
            $gate->define($permission->name . '.' . $permission->type, function (User $user) use ($permission) {
                return $user->hasPermissionTo($permission);
            });
        });

         */  // --- /Old ---

        // --- New ---

        $new_permissions = $this->getPermissionsNew();

        $new_permissions->map(function ($permission) use ($gate) {
            $gate->define($permission->name . '.' . $permission->type, function (User $user) use ($permission) {
                return $user->hasPermissionToNew($permission);
            });
        });
        // --- / New ---

        $gate->after([$this, 'createPermissionIfNotExists']);
    }

    /**
     * Get Permissions
     *
     * @return mixed
     */
    protected function getPermissionsNew()
    {
        try {
            $permissions = \DB::connection('cms')->table('user_permissions')
                ->select('*')
                ->get();

            return $permissions;

        } catch (\PDOException $m) {
            return new Collection();
        }
    }
    /**
     * Get Permissions
     *
     * @return mixed
     */
    protected function getPermissions()
    {
        try {
            return app(Permission::class)->with('roles')->get();
        } catch (\PDOException $m) {
            return new Collection();
        }
    }

    /**
     * Create Permission If Not Exists
     *
     * @param $user
     * @param $ability
     * @param $result
     */
    public function createPermissionIfNotExists($user, $ability, $result)
    {
        if ($result) {
            return;
        }

        if (strpos($ability, '.') === false) {
            return;
        }

        try {
            Permission::findByName($ability);
        } catch (\Exception $m) {
            $this->createPermissionFromAbility($ability);
        }
    }

    /**
     * Create Permission From Ability
     *
     * @param $ability
     */
    protected function createPermissionFromAbility($ability)
    {
        list($type, $name) = $this->extractNameTypeFromAbility($ability);

        if (!in_array($type, $this->types)) {
            return;
        }

        $this->saveNewPermission($name, $type);
    }

    /**
     * Extract Name Type From Ability
     *
     * @param $ability
     *
     * @return array
     */
    private function extractNameTypeFromAbility($ability)
    {
        $ability_array = explode('.', $ability);
        $type          = $ability_array[count($ability_array) - 1];
        $name          = substr($ability, 0, (strlen('.' . $type) * -1));

        return [$type, $name];
    }

    /**
     * Save New Permission
     *
     * @param $name
     * @param $type
     */
    protected function saveNewPermission($name, $type)
    {
        $permission       = new Permission();
        $permission->name = $name;
        $permission->type = $type;
        $permission->save();
    }
}
