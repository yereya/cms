@include('partials.containers.ajax-modal', [
            'part' => 'open',
            'title' => 'Edit Widget Template Blade',
            'event' => 'widget-data-changed',
            'ajax_submit' => true
        ])



{!! Form::open([
        'route' => ['sites.{site}.widgets.{widget}.templates.update-blade',
            $site->id, $widget_data->widget_id, 'widget_template_id' => $site_widget_template->id],
        'method' => 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
]) !!}

{{--todo return to this place when activate the htmlValidation --}}
{{--@include('partials.containers.validation.blade-diff')--}}
@include('partials.fields.textarea', [
            'name' => 'blade',
            'required' => 'true',
            'no_label' => false,
            'value' => $site_widget_template->blade,
            'attributes'=> [
                'data-validate' => false
            ]
        ])

{{--todo return to this place when activate the htmlValidation --}}
{{--{!! Form::hidden('close_modal', 0) !!}--}}
{{--{!! Form::close() !!}--}}

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'save' => true,
        'close' => true,
    ],
    'js' => [
        'modal_width' => 900,
        'code_mirror' => true
    ]
])