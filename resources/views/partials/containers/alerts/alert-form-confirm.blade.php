{!!Form::open([
    'url' => route($route, $route_params),
    'method' => $method ?? 'POST',
    'class' => 'form-invisible'
])!!}
<p>{!!  $text or 'Do you confirm performing this action?' !!} </p>
<button type="submit" class="btn btn-lg red">{{$btn_text ?? 'Yes'}}</button>
<a class="btn btn-lg green" onclick="swal.close()">Cancel</a>
{!! Form::close() !!}

