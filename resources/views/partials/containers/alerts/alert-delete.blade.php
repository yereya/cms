@include('partials.containers.alerts.alert-form-confirm',[
    'route' => $route,
    'route_params' => $route_params ?? '',
    'btn_text' => "Delete",
    'method' => 'delete',
    'text' => $text
])
