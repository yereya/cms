@include('partials.containers.dropdown-notifications-icon', [
    'notifications_unread' => $notifications_unread
])

<ul class="dropdown-menu">

    @include('partials.containers.dropdown-notifications-header', [
        'notifications_read'    => $notifications_read,
        'notifications_unread'  => $notifications_unread
    ])

    <li>
        <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
            @foreach ($notifications_to_show as $notification)
                <li>
                    <a href="{{ route('reports.alerts.{notification_id}.ajax-modal',[
                        'notification_id' => $notification['id'],
                        'method' => 'alertModal',
                    ]) }}"
                       data-toggle='modal' data-target='#ajax-modal' data-notification-id='{!! $notification['id'] !!}'>
                        {{--<span class="time">{{  }}</span>--}}
                        <span class="details">
                            <span class="label label-sm label-icon label-success">
                                <i class=" {!! $notification['alert_type']['icon'] !!} "></i>
                            </span>
                            {!! $notification['description'] !!}
                        </span>
                    </a>
                </li>
            @endforeach
        </ul>
    </li>

</ul>