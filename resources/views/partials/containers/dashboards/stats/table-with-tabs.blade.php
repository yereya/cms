@include('partials.containers.portlet', [
           'part' => 'open',
           'title'=>  $title,
   ])

<div class="portlet-body">
    <ul class="nav nav-tabs">
        @if(is_array($headers) && !empty($headers))
            @foreach($headers as $idx => $header)
                <li class="{{ !empty($header['active'])? 'active':'' }} " data-toggle="tab"
                    aria-expanded="false">
                    <a href="#tab_1_{{$idx}}" data-toggle="tab">
                        {{$header['label'] ?? ''}}
                    </a>
                </li>
            @endforeach
        @endif
    </ul>
    <div class="tab-content">
        @if(!empty($body))
            @foreach($body as $idx => $content)
                <div class="tab-pane {{$effect ?? 'fade' }} {{ !empty($content['active']) ?  'active in' : '' }} {{$content['class'] ?? ''}}"
                     id="tab_1_{{$idx}}">
                    {!! Form::open([
                          'route'=>[!empty($data['route']) ? $data['route'] : '' ],
                          'method' => 'POST',
                          'role' => 'form',
                          'class'=>'table-stats-form',
                          'data-controllers'=>'dash-params-form'
                       ])
                    !!}

                    {{csrf_field()}}
                    @foreach($data as $key=>$val)
                        {!! Form::hidden($key,$val) !!}
                        @if($val == 'alerts')
                            {{ Form::hidden('table_name',$content['table']) }}
                        @endif
                    @endforeach
                    @if(!empty($content['data']))
                        @foreach($content['data'] as $name=>$value)
                            {{ Form::hidden($name,$value) }}
                        @endforeach
                    @endif
                    @if(!empty($filters))
                        @foreach($filters as $key=>$filter)
                            {!! Form::hidden($key,$filter) !!}
                        @endforeach
                    @endif

                    <div class="table-stats  {{$wrapper_class ?? ''}}">
                        @if(!empty($filters))
                            @include('partials.containers.dashboards.stats.filters', ['filters'=>$filters])
                        @endif
                        <div class="table-responsive"></div>
                    </div>
                    <div class="clearfix"></div>
                    {!! Form::close() !!}
                </div>
            @endforeach
        @endif
    </div>
</div>

{!! Form::open([
    'route'=>[!empty($data['route-save-state']) ? $data['route-save-state'] : '' ],
                          'method' => 'POST',
                          'role' => 'form',
                          'class'=>'table-resolve-state-form'
]) !!}
{{csrf_field()}}
{!! Form::hidden('state') !!}
{!! Form::hidden('user_id',auth()->user()->id) !!}
{!! Form::hidden('record_id') !!}

{!! Form::close() !!}

@include('partials.containers.portlet',[
    'part'=>'close'
])