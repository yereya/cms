<?php
if (!is_null($field->metadata) && is_string($field->metadata)) {
    $metadata = json_decode($field->metadata);

    $select = [];
    $default = 0;
    if (isset($metadata->select)) {
        foreach($metadata->select as $option) {
            $select[] = ['id' => trim($option->key), 'text' => $option->value] ;
            if ($option->default) {
                $default = $option->priority;
            }
        }
    }
}
?>

<div class="col-md-offset-2">
    @include('partials.fields.h4-separator', ['label' => $field->display_name])
    <div class="column sortable">
        @if(!empty($select))
            @include('partials.fields.select', [
                 'label' => 'Options',
                 'name' => $field->name,
                 'list' => $select,
                 'value' => $data ?? $default,
            ])
        @endif

    </div>
</div>

