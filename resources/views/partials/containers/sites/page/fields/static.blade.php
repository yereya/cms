@include('partials.fields.select', [
    'label' => 'Desktop Template',
    'name' => 'template_id',
    'value' => $post->page->template_id ?? -1,
    'list' => $templates
])

@include('partials.fields.select', [
    'label' => 'Mobile Template',
    'name' => 'template_mobile_id',
    'value' => $post->page->template_mobile_id ?? -1,
    'list' => $templates
])

<script>
    (function () {
        topApp.sanitized();
        App.initSelect2ToSelects();
    })();
</script>