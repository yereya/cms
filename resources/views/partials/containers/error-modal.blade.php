@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'User Permissions',
])

    <h2>{{ $error_msg or 'Error' }}</h2>

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'js' => [
        'datatable' => [
            'class' => 'permissions-user'
        ]
    ]

])



