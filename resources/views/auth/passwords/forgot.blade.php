@extends('layouts.base')

@section('content')
    <div class="logo">
        <a href="{{ route('home') }}">
            <img src="{{ asset('assets/pages/img/logo-big.png') }}" alt="{{ Layout::getPageTitle() }}"/>
        </a>
    </div>

    <div class="content">
        <!-- BEGIN FORGOT PASSWORD FORM -->
        <form class="forget-form" action="{{ route('forgot-password-form') }}" method="post">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <button class="close" data-close="alert"></button>
                    <div> {{ Session::get('success') }} </div>
                </div>
            @endif

            @if($errors->count())
                <div class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    @foreach($errors->all() as $error)
                        <div> {{ $error }} </div>
                    @endforeach
                </div>
            @endif

            {!! csrf_field() !!}

                @if($errors->count())
                    <h3>Replace Your Password!!</h3>
                @else
                    <h3>Forget Password ?</h3>
                @endif


            <p> Enter your e-mail address below to reset your password. </p>

            <div class="form-group">
                <div class="input-icon">
                    <i class="fa fa-envelope"></i>
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email"
                           name="email"/>
                </div>
            </div>
            <div class="form-actions">
                <button type="button" class="btn red btn-outline" onclick="window.location.href='{{route('login')}}'">Back</button>
                <button type="submit" class="btn green pull-right"> Submit</button>
            </div>
        </form>
    </div>

    <div class="copyright">
        <span id="this-year"></span> &copy; Traffic Point LTD.
    </div>

    <script>
        var d = new Date();
        document.getElementById('this-year').innerHTML = d.getFullYear();
    </script>
@stop