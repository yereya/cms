@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'Widget',
    'event' => 'widget-changed',
])

@foreach($widgets as $widget)
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => $widget->name,
        'actions' => [
            [
                'target' => route('sites.{site}.widgets.ajax-modal', [$site->id, 'template'=>$template->id, 'method' => 'createOrEdit', 'widget_id' => $widget->id, 'column_id' => $column_id]),
                'type'   => 'modal',
                'title'  => 'Create',
                'icon'   => 'fa fa-plus',
                'class'  => 'create-widget-data',
                'modal_method' => 'open'
            ]
        ]
    ])

    @include('partials.containers.data-table', [
        'id' => 'widgets-list-table',
        'data' => $widget->widgetData,
        'columns' => [
            [
                'key' => 'Name',
                'value' => function ($widget_data) {
                    return $widget_data->name;
                }
            ],
            [
                'key' => 'actions',
                'label' => 'Actions',
                'width' => 250,
                'filter_type' => 'null',
                'value' => function ($widget_data) use ($site, $template, $column_id) {
                    $res = '';

                    $res .= '<button type="button" data-column-id="' . $column_id . '" data-id="'. $widget_data->id .'" data-data="' . e($widget_data->toJSON()) . '" class="action btn btn-xs btn-circle btn-primary choose-widget-data">
                                <i class="fa fa-rocket" aria-hidden="true"></i> Choose
                            </button>';

                    return $res;
                }
            ]

        ]
    ])

    @include('partials.containers.portlet', ['part' => 'close'])
@endforeach

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true
    ],
    'js' => [
        'datatable' => true
    ]
])