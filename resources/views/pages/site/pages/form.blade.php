@extends('layouts.metronic.main')

@section('page_content')
    <div class="row">
        <div class="col-md-9">

            @include('partials.containers.portlet', [
                'part' => 'open',
                'title' => 'Site Page Form',
                'has_form' => true,
                'actions' => [
                   isset($post) ?
                    [
                        'icon' => 'fa fa-cog',
                        'target' => route('sites.{site}.settings.ajax-modal', [$site->id, 'method=showModal', 'type='.SiteSetting::getPageType($post->id)]),
                        'attributes' => [
                            'data-toggle' => 'modal',
                            'data-target' => '#ajax-modal',
                        ]
                    ]: null,
                ]
            ])

            {!! Form::open([
                'route' => isset($post) ? ['sites.{site}.pages.update', $site->id, $post->id] : ['sites.{site}.pages.store', $site->id],
                'method' => isset($post) ? 'PUT' : 'POST',
                'class' => 'form-horizontal',
                'role' => 'form'
            ]) !!}

            {{ Form::hidden('parent_id', $parent_id ?? 0) }}

            <div class="form-body">
                @include('partials.fields.input', [
                    'label' => 'Title',
                    'name' => 'name',
                    'required' => 'true',
                    'value' => $post->name ?? ''
                ])

                @include('partials.fields.input', [
                    'label' => 'Slug',
                    'name' => 'slug',
                    'required' => true,
                    'addon' => !empty($slug) ? 'https://' . $slug : '',
                    'addon_right_href' => isset($post) ? $slug . $post->slug : false,
                    'value' => $post->slug ?? null,
                    'attributes' => [
                        'data-sanitizer_watch' => 'input[name=name]',
                        'data-sanitizer_method' => isset($post) ?: 'toSlug'
                    ]
                ])

                @include('partials.fields.select', [
                    'label' => 'Page Type',
                    'name' => 'type',
                    'required' => 'true',
                    'class' => 'choice_type',
                    'value' => $post->page->type ?? 'static',
                    'list' => $page_types,
                    'attributes' => [
                        'data-route' => route('sites.{site}.pages.choice', [
                            $site->id,
                            'method' => 'pageType',
                            'parent_id' => $post->parent_id ?? 0,
                            'page' => $post->id ?? 0
                        ]),
                    ]
                ])

                <fieldset id="field-metadata"></fieldset>
            </div>

            @include('partials.containers.form-footer-actions', [
                'buttons' => [
                    'cancel' => [
                        'route' => back_url(['site'=> $site->id])
                    ],
                    'submit' => 'true',
                ],
                'checkboxes' => [
                    'create_another' => isset($post) ? null : 'true'
                ]
            ])

            @include('partials.containers.sites.page.seo-portlet',[
                'seo_settings' => $post ?? null
            ])

            @include('partials.containers.portlet', [
                'part' => 'close'
            ])

        </div>

        @include('pages.site.pages.sidebar-portlets.form-side-bar', [
          'post' => $post ?? null
        ])
    </div>


    {!! Form::close() !!}

@stop