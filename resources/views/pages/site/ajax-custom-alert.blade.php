Please confirm deleting page <b>{{$name}}</b>
<br />
@if(isset($sub_list) && count($sub_list))
    With children pages
    <br>
    <ul style="list-style-type: none">
    @foreach($sub_list as $sub_name)
       <li>{{$sub_name}}</li>
    @endforeach
    </ul>
@endif

