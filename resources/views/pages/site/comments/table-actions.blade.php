@if($can['edit'])
    <a href="{{route('sites.{site}.comments.edit', [$site_id, $data['id']]) }}">
        <span aria-hidden="true" class="icon-pencil tooltips" title="Edit comment"></span>
    </a>

    <a href="{{route('sites.{site}.comments.create', [$site_id, "replay_to=".$data['id']]) }}">
        <span aria-hidden="true" class="icon-share-alt tooltips" title="replay to"></span>
    </a>
@endif
