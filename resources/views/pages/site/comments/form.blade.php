@extends('layouts.metronic.main')

@section('page_content')

    {!! Form::open([
        'route' => isset($comment) ? ['sites.{site}.comments.update', $site->id, $comment->id] : ['sites.{site}.comments.store', $site->id],
        'method' => isset($comment) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}


    @if (isset($comment_to_replay))
        @include('pages.site.comments.replay-to-comment')
        {!! Form::hidden('parent_id', $comment_to_replay->id ?? null) !!}
        {!! Form::hidden('post_id', $comment_to_replay->post_id) !!}
    @endif

    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Comment Form',
        'has_form' => true
    ])

    @if (isset($content_types))
        @include('partials.fields.select', [
            'name' => 'content_type_id',
            'id' => 'post-content-type-list',
            'required' => true,
            'label' => 'Content Types',
            'class' => 'pull-right',
            'list' => $content_types,
            'value' => request()->get('content_type_id')
        ])

        @include('partials.fields.select', [
            'name' => 'post_id',
            'label' => 'Post',
            'required' => true,
            'class' => 'pull-right',
            'ajax' => true,
            'attributes' => [
                'data-min_input' => 0,
                'data-dependency_selector[0]' => '#post-content-type-list',
                'data-method' => 'list',
                'data-api_url' => route('sites.{site}.posts.select2',[$site->id]),
                'data-value' => '',
            ]
        ])
    @endif

    @include('partials.fields.input', [
        'name' => 'author',
        'required' => 'true',
        'value' => $comment->author ?? null
    ])

    @include('partials.fields.input', [
        'name' => 'email',
        'required' => 'true',
        'value' => $comment->email ?? null
    ])

    @include('partials.fields.select', [
        'name' => 'status',
        'required' => 'true',
        'list' => $status_options,
        'value' => $comment->status ?? null
    ])

    @include('partials.fields.checkbox', [
        'label' => 'Visible',
        'wrapper' => true,
        'name' => 'visible',
        'list' => [
            [
                'name' => 'visible',
                'checked' => $comment->visible ?? 0
            ]
        ]
    ])

    @include('partials.fields.select', [
        'name' => 'rating_overall',
        'list' => [1 => 1, 2 => 2, 3 => 3, 4 => 4 ,5 => 5 ,6 => 6 ,7 => 7 ,8 => 8, 9 => 9 ,10 => 10],
        'value' => $comment->rating_overall ?? null
    ])

    @for ($i = 1;$i<7;$i++)
        @php($key = 'rating_' . $i)
        @include('partials.fields.select', [
            'name' => $key,
            'list' => [1 => 1, 2 => 2, 3 => 3, 4 => 4 ,5 => 5 ,6 => 6 ,7 => 7 ,8 => 8, 9 => 9 ,10 => 10],
            'value' => $comment->{$key} ?? null
        ])
    @endfor

    @if(isset($comment))
        @if(isset($comment->content))
            @foreach($comment->content as $name => $value)
                @include('partials.fields.textarea', [
                    'label' => ucfirst($name),
                    'name' => 'content['. $name .']',
                    'value' => $value ?? null
                ])
            @endforeach
        @endif
    @else
        @include('partials.fields.select', [
            'label' => 'Comment type',
            'name' => 'type',
            'required' => 'true',
            'force_selection' => true,
            'class' => 'choice_type',
            'value' => 1,
            'list' => $types,
            'attributes' => [
                'data-route' => route('sites.{site}.comments.choice', [
                    $site->id,
                    'method' => 'type',
                ]),
            ]
        ])
    @endif

    <fieldset id="field-metadata"></fieldset>


    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => route('sites.{site}.comments.index', [$site->id]),
            ],
            'submit' => 'true'
        ],
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])

    {!! Form::close() !!}

@stop