@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Contact Us',
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New',
                'target' => route('sites.{site}.contact-us.create', [$site_id]),
            ]
        ]
    ])

    @include('partials.containers.data-table', [
        'data' => [],
        'id' => 'data-table-contact-us',
        'server_side' => true,
        'url' => route('sites.{site}.contact-us.datatable',[$site_id]),
        'attributes' => [
            'data-filter_container' => 'table-filters',
            'data-onchange_submit' => 'true',
            'data-page-length' => '20',
            'data-route_method' => 'list',

            //the "processing" loader
            'data-blocking_loader' => 'true',
        ],
        'columns' => [
          'id',
          'name',
          'Email',
          'message',
          'Created at',
          'Updated at',
          'Actions'
        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop
