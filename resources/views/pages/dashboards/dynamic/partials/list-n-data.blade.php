<div class="col-md-9">
    <div class="row">

        {{--ALERTS TREE--}}
        <div class="col-lg-7 col-md-12">
            <div id="treeWrapper" class="dynamic-tree">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="btn-toolbar margin-bottom-10">
                            <div class="btn-group btn-group-sm btn-group-solid listType-wrapper">
                                <button type="button" class="btn grey active" data-type="flat">Flat
                                    View
                                </button>
                                <button type="button" class="btn grey" data-type="marked">Marked
                                </button>
                                <button type="button" class="btn grey" data-type="hierarchy">
                                    Hierarchy
                                </button>
                            </div>
                            <div class="btn-group grey btn-group-sm btn-group-solid sort-by-buttons"
                                 style="float:right">
                                <button type="button" class="btn default">Sort By</button>
                                <button type="button" class="btn default dropdown-toggle"
                                        data-toggle="dropdown"
                                        aria-expanded="false" role="button">
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li data-sort="default" class="active">
                                        <a href="javascript:;"> Default </a>
                                    </li>
                                    <li data-sort="profit" data-direction="asc">
                                        <a href="javascript:;"> Profit - High To Low </a>
                                    </li>
                                    <li data-sort="profit" data-direction="desc">
                                        <a href="javascript:;"> Profit - Low To High </a>
                                    </li>
                                    <li data-sort="alert_days" data-direction="asc">
                                        <a href="javascript:;"> Days - High To Low </a>
                                    </li>
                                    <li data-sort="alert_days" data-direction="desc">
                                        <a href="javascript:;"> Days - Low To High </a>
                                    </li>
                                    <li data-sort="account_name" data-direction="asc">
                                        <a href="javascript:;"> Account </a>
                                    </li>
                                    <li data-sort="campaign_name" data-direction="asc">
                                        <a href="javascript:;"> Campaign </a>
                                    </li>
                                    <li data-sort="ad_gourp_name" data-direction="asc">
                                        <a href="javascript:;"> Ad Group </a>
                                    </li>
                                    <li data-sort="token" data-direction="asc">
                                        <a href="javascript:"> Token </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="tabbable tabbable-tabdrop">
                        <ul class="nav nav-pills alert-selection">
                            <li class="individual col-xs-offset-3 active">
                                <a href="#tab1" data-toggle="tab" aria-expanded="false"
                                   class="btn btn-default">
                                    Select One Item
                                </a>
                            </li>
                            <li class="multi">
                                <a href="#tab2" data-toggle="tab" aria-expanded="true"
                                   class="btn btn-default">
                                    Select Multiple Items
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="portlet-body tree" style="max-height: 1000px; overflow-y: scroll;">
                        {{--Advertiser Level--}}
                        <ol id="tree" class="dd-list dynamic-alerts-list"></ol>
                    </div>
                </div>
            </div>
        </div>

        {{--ALERT STATS AND STATE--}}
        <div class="col-lg-5 col-md-12">
            <div class="portlet light bordered stats">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bubble font-red"></i>
                        <span class="caption-subject font-red sbold uppercase">Alert: </span>
                        <span class="alert-level"></span>
                        <span class="alert-type"></span>
                    </div>
                </div>
                <div class="portlet-body">

                    <div class="top-content-stats collapse">
                        <div class="breadcrumbs-list col-md-7">
                            <ul class="clearfix">
                                <li class="item advertiser">
                                    <span class="description">Advertiser: </span>
                                    <span class="name"></span>
                                </li>
                                <li class="item account">
                                    <span class="description">Account: </span>
                                    <span class="name"></span>
                                </li>
                                <li class="item campaign">
                                    <span class="description">Campaign: </span>
                                    <span class="name"></span>
                                </li>
                                <li class="item ad_group">
                                    <span class="description">Ad Group: </span>
                                    <span class="name"></span>
                                </li>
                                <li class="item mt">
                                    <span class="description">Match Type: </span>
                                    <span class="name"></span>
                                </li>
                                <li class="item device">
                                    <span class="description">Device: </span>
                                    <span class="name"></span>
                                </li>
                                <li class="item keyword">
                                    <span class="description">Keyword: </span>
                                    <span class="name"></span>
                                </li>
                            </ul>
                        </div>
                        <div class="overview-wrapper col-md-5">
                            <ul class="overview">
                                <li class="profit field">
                                    <span class="description">Profit:</span>
                                    <span class="amount"></span>
                                </li>
                                <li class="cost field">
                                    <span class="description">Cost:</span>
                                    <span class="amount"></span>
                                </li>
                                <li class="creation_date field">
                                    <span class="description">Creation Date:</span>
                                    <span class="amount"></span>
                                </li>
                                <li class="alert_days field">
                                    <span class="description">Alert Age:</span>
                                    <span class="amount"></span>
                                </li>
                                <li class="site_ctr field">
                                    <span class="description">Site Ctr:</span>
                                    <span class="amount"></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="table-responsive">
                        <table class="table alert-records">
                            <thead>
                            <tr>
                                <th class="uppercase">Metric</th>
                                <th class="uppercase">current</th>
                                <th class="uppercase">history</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="cpc">
                                <td><span class="font-grey-salt">CPC</span></td>
                                <td class="current">-</td>
                                <td class="history">-</td>
                            </tr>
                            <tr class="cpa">
                                <td><span class="font-grey-salt">CPA</span></td>
                                <td class="current">-</td>
                                <td class="history">-</td>
                            </tr>
                            <tr class="cts">
                                <td><span class="font-grey-salt">CTS</span></td>
                                <td class="current">-</td>
                                <td class="history">-</td>
                            </tr>
                            <tr class="profit">
                                <td><span class="font-grey-salt">Profit</span></td>
                                <td class="current">-</td>
                                <td class="history">-</td>
                            </tr>
                            <tr class="site_ctr">
                                <td><span class="font-grey-salt">Site CTR</span></td>
                                <td class="current">-</td>
                                <td class="history">-</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    {{--OPEN FORM SAVE STATES FIELDS--}}
                    @include('pages.dashboards.dynamic.partials.forms.states.open')

                    <div class="form-group description">
                        <input class="form-control text-center"
                               type="text" name="description"
                               placeholder="Explain your action.">
                    </div>
                    <div class="buttons">
                        <button href="javascript:void(0);" class="btn  btn-default prev"
                                data-type="prev">Prev
                        </button>
                        <button href="javascript:void(0);" name="state" value="ignore"
                                class="btn grey ignore"
                                data-resolve="ignored">No action
                        </button>
                        <button href="javascript:void(0);" name="state" value="fixed"
                                class="btn green fixed"
                                data-resolve="fixed">Took action
                        </button>
                        <button href="javascript:void(0);" class="btn  btn-default next"
                                data-type="next">Next
                        </button>
                    </div>

                    {{--CLOSE FORM--}}
                    @include('pages.dashboards.dynamic.partials.forms.states.close')

                </div>
            </div>
        </div>
    </div>
</div>