<div class="portlet-title tabbable-line ">
    <div class="caption">
        <i aria-hidden="true" class="icon-notebook"></i>
        <span class="caption-subject font-dark bold uppercase">Offense/Defense</span>
    </div>
</div>

@include('partials.containers.widget-row', [
     'column_size'=> 6,
     'class'=>'alert_types',
     'data'=> [
         [
         'title'    => 'Offense',
         'class'=>'highCTS',
         'attributes'=> [
             'data-filter'=>'highCTS',
             'data-group'=> 'alert_types',
             'clickable'=> true
         ],

         'content'=> [
               'value'=>'Alerts Sum',
               'color'=>'',
               'length'=> true
             ]
         ],
          [
         'title'   => 'Defense',
         'class'=>'lowCTS',
         'attributes'=>[
             'data-filter'=>'lowCTS',
             'clickable'=> true,
             'data-group'=>'alert_types'
         ],

         'content'=> [
               'value'=>'Alerts Sum',
               'color'=>'',
               'length'=> true
             ]
         ],
       ]
    ])
