@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Archive',
        'actions' => [
                [
                    'drop_down' => true,
                    'title' => 'Actions',
                    'icon' => 'fa fa-th',
                    'menus' => [
                        [
                            'container' => true,
                            'context' => [
                                'include' => 'partials.containers.buttons.download',
                                'route' => ['reports.logs.datatable'],
                                'title' => 'Download CSV'
                            ]
                        ]
                    ]
                ]
            ]
    ])

    @include('pages.reports.logs.filters.table-index')
    @include('partials.containers.data-table', [
        'data' => [],
        'id' => 'tp-log',
        'url' => route('reports.logs.datatable', ['url_query' => $url_query]),
        'server_side' => true,
        'attributes' => [
            'data-filter_container' => 'table-filters',
            'data-page-length' => '20',
            'data-order' => '[[0, "desc"]]',
            'data-onchange_submit' => 'true',
            'data-route_method' => 'logQuery',
            'data-search' => 'search',
            'data-scrollX' => 'true',

            //the "processing" loader
            'data-blocking_loader' => 'true'
        ],
        'columns' => ['Recorded At', 'Run result', 'System', 'Task Id', 'Message', 'Run Time', 'Initiator', 'actions']
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop