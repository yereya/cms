@extends('layouts.metronic.main')

@section('page_title')
    <small>Publisher</small> {{ $publisher->publisher_name }} <small>Media</small>
    @if (isset($media))
        {{ $media->media_name }}
    @endif
@endsection

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => isset($media) ? 'Edit' : 'Create'
    ])

    {!! Form::open([
        'route' => isset($media) ? ['bo.publishers.{publisher_id}.accounts.update', $publisher->id, $media->id] : ['bo.publishers.{publisher_id}.accounts.store', $publisher->id],
        'method' => isset($media) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}

    @include('partials.fields.input', [
            'name' => 'name',
            'required' => true,
            'value' => isset($media) ? $media->name : null
        ])

    @include('partials.fields.select', [
        'name' => 'type',
        'required' => true,
        'label' => 'Type',
        'list' => ['Mobile App' => 'Mobile App', 'Table App' => 'Table App', 'Web Site' => 'Web Site', 'Network' => 'Network', 'AdWords' => 'AdWords'],
        'value' => isset($media) ? $media->type : ''
    ])

    @include('partials.fields.select', [
        'name' => 'status',
        'required' => true,
        'label' => 'Status',
        'list' => ['Active' => 'Active', 'Inactive' => 'Inactive', 'Pending' => 'Pending'],
        'value' => isset($media) ? $media->status : ''
    ])

    @include('partials.fields.input', [
        'name' => 'url',
        'value' => isset($media) ? $media->url : ''
    ])

    @if(isset($publisher))
        @include('partials.containers.form-footer-actions', [
                'buttons' => [
                    'cancel' => [
                        'route' => back_url()
                    ],
                    'submit' => 'true'
                ]
            ])
    @else
        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'cancel' => [
                    'route' => back_url()
                ],
                    'submit' => 'true'
                ],
                'checkboxes' => [
                    'create_another' => isset($role) ? null : 'true'
                ]
            ])
    @endif


    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop


