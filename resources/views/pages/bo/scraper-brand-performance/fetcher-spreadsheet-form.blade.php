<div class="row">
    <div class="properties_fetcher">
        @include('partials.fields.h4-separator', ['label' => 'Spreadsheet info API',])

        @if(isset($fetcher->spreadsheet_info) && count($fetcher->spreadsheet_info) > 0)
            @foreach($fetcher->spreadsheet_info as $info)
                @include('partials.containers.duplicate-group', ['part' => 'open', 'offset' => 1])

                @include('partials.fields.select', [
                    'name' => 'spreadsheet_info[key][]',
                    'label' => '',
                    'id' => null,
                    'no_label' => true,
                    'form_line' => true,
                    'column' => 5,
                    'column_label' => 0,
                    'class' => 'key_group',
                    'list' => ['spreadsheet_id' => 'spreadsheet id', 'fields' => 'fields', 'key' => 'key', 'excluded_gids' => 'Excluded gids', 'allowed_gids' => 'Allowed gids'],
                    'value' => [$info['key'] ?? '']
                ])

                @include('partials.fields.input', [
                    'name' => 'spreadsheet_info[value][]',
                    'label' => false,
                    'placeholder' => 'Value',
                    'form_line' => true,
                    'column' => 5,
                    'column_label' => 0,
                    'value' => $info['value']
                ])

                @include('partials.containers.actions', [
                    'class' => true,
                    'actions' => [
                        [
                            'action' => 'clone',
                        ],
                        [
                            'action' => 'remove',
                        ]
                    ]
                ])

                @include('partials.containers.duplicate-group', ['part' => 'close', 'offset' => true])
            @endforeach
        @else
            @include('partials.containers.duplicate-group', ['part' => 'open', 'offset' => 1])

            @include('partials.fields.select', [
                'name' => 'spreadsheet_info[key][]',
                'label' => '',
                'id' => null,
                'no_label' => true,
                'form_line' => true,
                'column' => 5,
                'column_label' => 0,
                'class' => 'key_group',
                'list' => ['spreadsheet_id' => 'spreadsheet id', 'fields' => 'fields', 'key' => 'key', 'excluded_gids' => 'Excluded gids', 'allowed_gids' => 'Allowed gids'],
                'value' => ['']
            ])

            @include('partials.fields.input', [
                'name' => 'spreadsheet_info[value][]',
                'label' => false,
                'placeholder' => 'Value',
                'form_line' => true,
                'column' => 5,
                'column_label' => 0,
                'value' => ''
            ])

            @include('partials.containers.actions', [
                'class' => true,
                'actions' => [
                    [
                        'action' => 'clone',
                    ],
                    [
                        'action' => 'remove',
                    ]
                ]
            ])

            @include('partials.containers.duplicate-group', ['part' => 'close', 'offset' => true])
        @endif
    </div>
</div>
