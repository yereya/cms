@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
            'part' => 'open',
            'title' => 'Placements',
            'actions' => [
                [
                    'icon' => 'fa fa-plus',
                    'title' => 'Add New Placement',
                    'target' => route('bo.publishers.{publisher}.medias.{medias}.placements.create', [$publisher->id, $media->id])
                ]
            ]
        ])

    @include('partials.containers.data-table', [
           'data' => $placements,
           'columns' => [
               [
                   'key' => 'placement_name',
                   'value' => function ($data) {
                       return $data->placement_name;
                   }
               ],
               [
                   'key' => 'type',
                   'value' => function ($data) {
                        $status = strtolower($data['type']);
                        $class = 'label-success';

                        if ($status == 'inactive') {
                            $class = 'label-danger';
                        } elseif ($status == 'pending') {
                            $class = 'label-info';
                        }

                        return "<span class='label label-sm $class'>$status</span>";
                   }
               ],
               [
                   'key' => 'mongodb_id',
                   'value' => function ($data) {
                       return $data->mongodb_id;
                   }
               ],
               [
                   'key' => 'actions',
                   'value' => function ($data) use ($publisher, $media, $permission_name) {
                       $res = '';

                       if (auth()->user()->can($permission_name .'.edit')) {
                            $res .= "<a href='" . route('bo.publishers.{publisher}.medias.{medias}.placements.edit', [$publisher->id, $media->id, $data->id]) . "' class='tooltips' data-original-title='Edit placement'><span aria-hidden='true' class='icon-pencil fa-fw'></span></a>";
                            $res .= "<a href='" . route('bo.publishers.{publisher}.medias.{medias}.placements.{placements}.links', [$publisher->id, $media->id, $data->id]) . "' class='tooltips' data-original-title='Show links' title='Show links' data-toggle='modal' data-target='#ajax-modal'><i aria-hidden='true' class='fa fa-link fa-fw'></i></a>";
                       }

                       return $res;
                   }
               ]
           ]
        ])

    @include('partials.containers.portlet', [
    'part' => 'close'
])
@stop
