@extends('layouts.metronic.main')

@section('page_title')
@endsection

@section('page_content')
    @include('partials.containers.portlet', [
            'part' => 'open',
            'title' => 'Publishers',
            'actions' => [
                [
                    'icon' => 'fa fa-plus',
                    'title' => 'Add New Publisher',
                    'target' => route('bo.publishers.create'),
                    'permission' => auth()->user()->can($permission_name .'.add')
                ]
            ]
        ])

    @include('partials.containers.data-table', [
           'data' => $publishers,
           'attributes' => [
                'data-page-length' => '50',
                'data-order' => '[[2, "asc"]]'
           ],
           'columns' => [
               [
                   'key' => 'id',
                   'value' => function ($data) {
                       return $data->id;
                   }
               ],
               [
                   'key' => 'publisher_name',
                   'value' => function ($data) {
                       return  "<a href='#'><span>$data->publisher_name</span></a>";
                   }
               ],
               [
                   'key' => 'status',
                   'value' => function ($data) {
                        $status = strtolower($data->status);
                        $class = 'label-success';

                        if ($status == 'inactive') {
                            $class = 'label-danger';
                        } elseif ($status == 'pending') {
                            $class = 'label-info';
                        }

                        return "<span class='label label-sm $class'>$status</span>";
                   }
               ],
               [
                   'key' => 'type',
                   'value' => function ($data) {
                        $res = '';
                        if ($data->type) {
                            $class = strtolower($data->type) == 'normal' ? 'bg-blue-dark' : 'bg-blue-steel';
                            $res .= "<span class='label label-sm $class'>$data->type</span>";
                        }
                       return $res;
                   }
               ],
               [
                   'key' => 'actions',
                   'value' => function ($data) use ($permission_name) {
                       $res = '';

                        if (auth()->user()->can($permission_name .'.edit')) {
                            //$res .= "<a href='".route('bo.publishers.edit', $data->id)."' class='tooltips' data-original-title='Edit action'><span aria-hidden='true' class='icon-pencil'></span></a>";
                            $res .= "<a href='".route('bo.publishers.edit',  ['publisher=' . $data->id])."' class='tooltips' data-original-title='Edit action'><span aria-hidden='true' class='icon-pencil'></span></a>";
                        }

                        if (auth()->user()->can($permission_name .'.accounts.view')) {
                             $res .= "<a href='".route('bo.accounts.index', ['publisher=' . $data->id])."' class='tooltips' data-original-title='Show accounts'><span aria-hidden='true' class='fa fa-newspaper-o fa-fw'></span></a>";
                        }
                        /*
                        if (auth()->user()->can($permission_name . '.medias.view')) {
                            $res .= "<a href='" . route('bo.publishers.medias.index',['publisher=' . $data->id]) . "' class='tooltips' data-original-title='Show medias'><span aria-hidden='true' class='fa fa-tv fa-fw'></span></a>";
                        }
                        */
                       return $res;
                   }
               ]
           ]
        ])

    @include('partials.containers.portlet', [
    'part' => 'close'
])
@stop
