@extends('layouts.metronic.main')

@section('page_title')
    <small>{{ $brand->name }}</small>
@endsection

@section('page_content')

    <div class="row">
        <div class="col-md-12 col-lg-8">
            <div class="col-md-12">
                @include('partials.containers.portlet', [
                    'part' => 'open',
                    'title' => "Campaigns - ",
                    'subtitle' => $brand->name,
                    'actions' => [
                        [
                            'icon' => 'fa fa-plus',
                            'title' => 'Add New Campaign',
                            'target' => route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.create', [$advertiser->id, $brand->id])
                        ]
                    ]
                ])

                @include('pages.bo.advertisers.brands.campaign.filters.table-campaigns', ['permissions' => ['add' => auth()->user()->can($permission_name .'.add')]])

                @include('partials.containers.data-table', [
                   'data' => $campaigns,
                   'attributes' => [
                       'data-page-length' => 50
                   ],
                   'columns' => [
                       [
                           'key' => 'name',
                           'width' => '15%',
                           'value' => function ($data) use ($advertiser, $brand) {
                               return "<span class='label'><a href='".route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.{campaign_id}.lp.index', [$advertiser->id, $brand->id, $data->id])."'>$data->name</a></span>";
                           }
                       ],
                       [
                           'key' => 'status',
                           'width' => '7%',
                           'value' => function ($data) {
                                $status = strtolower($data->status);
                                $class = 'label-success';

                                if ($status == 'inactive') {
                                    $class = 'label-danger';
                                } elseif ($status == 'pending') {
                                    $class = 'label-info';
                                }

                                return "<span class='label label-sm $class'>$status</span>";
                           }
                       ],
                       [
                           'key' => 'actions',
                           'width' => '25%',
                           'filter_type' => 'null',
                           'value' => function ($data) use ($advertiser, $brand) {
                               $group_id = $data->groups[0]->id ?? null;

                               $res = '';

                               if ($group_id) {
                                   $res .= "<a href='".route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaign-groups.edit', [$advertiser->id, $brand->id, $group_id])."' class='tooltips' data-original-title='Edit Parameters'><span aria-hidden='true' class='btn btn-primary btn-sm'>Edit Parameters</span></a>";
                               }

                               $res .= "<a href='"
                                .route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.ajax-modal',
                                 [$advertiser->id, $brand->id, 'method' => 'placementLink', 'advertiser' => $advertiser->id, 'brand' => $brand->id, 'campaign' => $data->id , 'placement' => $data->placement->id ?? null, 'domain' => $advertiser->url ?? ''])
                                 ."' class='tooltips' data-toggle='modal' data-target='#ajax-modal' data-original-title='Link'><span class='btn btn-primary btn-sm'>Link</span></a>";
                               $res .= "<a href='".route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.{campaign_id}.lp.index', [$advertiser->id, $brand->id, $data->id])."' class='tooltips' data-original-title='Landing pages'><span class='btn btn-default btn-sm'>Landing pages</span></a>";


                               return $res;
                           }
                       ],
                   ]
                ])

                @include('partials.containers.portlet', [
                    'part' => 'close'
                ])
            </div>

            <div class="col-md-12">

                @include('partials.containers.portlet', [
                    'part' => 'open',
                    'title' => "Parameter Groups",
                    'actions' => [
                        [
                            'icon' => 'fa fa-plus',
                            'title' => 'Add New Group',
                            'target' => route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaign-groups.create', [$advertiser->id, $brand->id])
                        ]
                    ]
                ])

                    @include('partials.containers.data-table', [
                        'data' => $campaign_groups,
                        'attributes' => [
                            'data-page-length' => 50
                        ],
                        'columns' => [
                            [
                                'key' => 'name',
                                'value' => function ($data) use($advertiser, $brand) {
                                    return "<span class='label'><a href='".route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaign-groups.edit', [$advertiser->id, $brand->id, $data->id])."'>$data->name</a></span>";
                                }
                            ],
                            [
                                'key' => 'status',
                                'value' => function ($data) {
                                    $status = !empty($data->status) ? $data->status : 'active';
                                    $class = 'label-success';

                                    if ($data->status == 'inactive') {
                                        $class = 'label-danger';
                                    } elseif ($data->status == 'pending') {
                                        $class = 'label-warning';
                                    }

                                    return "<span class='label label-sm $class'>$status</span>";
                                }
                            ],
                            [
                                'key' => 'actions',
                                'filter_type' => 'null',
                                'value' => function ($data) use ($advertiser, $brand) {
                                    $group_id = $data->id ?? 1;
                                    $res = '';

                                    $res .= "<a href='".route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaign-groups.edit', [$advertiser->id, $brand->id, $group_id])."' class='tooltips' data-original-title='Edit'><span aria-hidden='true' class='btn btn-primary btn-sm icon-pencil'></span></a>";
                                    $res .= Form::open(['route' => ['bo.advertisers.{advertiser_id}.brands.{brand_id}.campaign-groups.destroy', $advertiser->id, $brand->id, $group_id], 'method' => 'delete', 'class' => 'form-invisible tooltips', 'data-original-title' => 'Delete']);
                                    $res .= '<button type="submit"><span aria-hidden="true" class="btn btn-danger btn-sm">Delete</span></button>';
                                    $res .= Form::close();

                                    return $res;
                                }
                            ]
                        ]
                    ])

                @include('partials.containers.portlet', [
                    'part' => 'close'
                ])
            </div>
        </div>

        <div class="col-md-12 col-lg-4">
            @include('partials.containers.portlet', [
                'part' => 'open',
                'title' => "Edit Brand - ",
                'subtitle' => $brand->name,
                'has_form' => true,
                'tools' => [
                    'collapse' => isset($brand) ? ['class' => 'expand'] : ['class' => ''],
                ]
            ])

            @include('pages.bo.advertisers.brands.io_included_form', ['advertiser' => $advertiser, 'brand' => $brand, 'route_param' => 'campaign_index'])

            @include('partials.containers.portlet', [
                'part' => 'close'
            ])
        </div>

        <div class="col-md-12">

            @include('partials.containers.portlet', [
                'part' => 'open',
                'title' => "Brands actions -",
                'subtitle' => $brand->name,
                'actions' => [
                    [
                        'icon' => 'fa fa-plus',
                        'title' => 'Add Pixel',
                        'target' => route('bo.advertisers.{advertiser_id}.brands.{brand_id}.actions.create', [$advertiser->id, $brand->id])
                    ]
                ]
            ])
            @include('partials.containers.data-table', [
                'data' => $brand_actions ?? [],
                'attributes' => [
                    'data-page-length' => 50
                ],
                'columns' => [
                    [
                        'key' => 'name',
                        'value' => function ($data) use($advertiser, $brand) {
                            return "<span class='label'><a href='".route('bo.advertisers.{advertiser_id}.brands.{brand_id}.actions.edit', [$advertiser->id, $brand->id, $data->id])."'>$data->name</a></span>";
                        }
                    ],
                    [
                        'key' => 'pixel',
                         'value' => function ($actionData) use($advertiser, $brand, $actions_url_queries) {
                            $domain = $advertiser->url ?? "out.trafficpointltd.com";
                            $trackUrlPath = "/track/action/{$actionData->type}/";
                            $actionQueryParams = htmlentities($actions_url_queries[$actionData->id] ?? '');
                            $pixelUrl = "{$domain}{$trackUrlPath}{$actionQueryParams}";
                            return "<span>$pixelUrl</span>";
                        }
                    ],
                    [
                        'key' => 'status',
                        'value' => function ($data) {
                            $status = !empty($data->status) ? $data->status : 'active';
                            $class = 'label-success';

                            if ($data->status == 'inactive') {
                                $class = 'label-danger';
                            } elseif ($data->status == 'pending') {
                                $class = 'label-warning';
                            }

                            return "<span class='label label-sm $class'>$status</span>";
                        }
                    ],
                    [
                        'key' => 'actions',
                        'filter_type' => 'null',
                        'value' => function ($data) use ($advertiser, $brand) {
                            $action_id = $data->id ?? 1;
                            $res = '';

                            $res .= "<a href='".route('bo.advertisers.{advertiser_id}.brands.{brand_id}.actions.edit', [$advertiser->id, $brand->id, $action_id])."' class='tooltips' data-original-title='Edit'><span aria-hidden='true' class='btn btn-primary btn-sm icon-pencil'></span></a>";
                            $res .= Form::open(['route' => ['bo.advertisers.{advertiser_id}.brands.{brand_id}.actions.destroy', $advertiser->id, $brand->id, $action_id], 'method' => 'delete', 'class' => 'form-invisible tooltips', 'data-original-title' => 'Delete']);
                            $res .= '<button type="submit"><span aria-hidden="true" class="btn btn-danger btn-sm">Delete</span></button>';
                            $res .= Form::close();

                            return $res;
                        }
                    ]
                ]
            ])

            @include('partials.containers.portlet', [
                'part' => 'close'
            ])
        </div>


    </div>

@stop