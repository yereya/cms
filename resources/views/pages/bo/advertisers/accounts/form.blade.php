@extends('layouts.metronic.main')

@section('page_title')
    <small>Advertiser</small> {{ $advertiser->name }} <small>Account</small>
    @if (isset($account)) {{ $account->name }} @endif
@endsection

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => isset($account) ? 'Edit' : 'Create'
    ])

    {!! Form::open([
            'route' => isset($account) ? ['bo.advertisers.{advertiser_id}.accounts.update', $advertiser->id, $account->id] : ['bo.advertisers.{advertiser_id}.accounts.store', $advertiser->id],
            'method' => isset($account) ? 'PUT' : 'POST',
            'class' => 'form-horizontal',
            'role' => 'form'
        ]) !!}

    @include('partials.fields.input', [
            'name' => 'name',
            'required' => true,
            'value' => isset($account) ? $account['name'] : null
        ])

    @include('partials.fields.input', [
            'name' => 'source_account_id',
            'required' => true,
            'value' => isset($account) ? $account['source_account_id'] : null
        ])

    @include('partials.fields.select', [
        'name' => 'type',
        'label' => 'Status',
        'required' => true,
        'list' => ['Active' => 'Active', 'Inactive' => 'Inactive', 'Pending' => 'Pending'],
        'value' => isset($account) ? $account->status : ''
    ])

    @include('partials.fields.select', [
        'name' => 'report',
        'multi_select' => true,
        'list' => \App\Entities\Models\Bo\Report::pluck('name', 'id'),
        'value' => isset($report) ? $report : ''
    ])

    @if(isset($account))
        @include('partials.containers.form-footer-actions', [
                'buttons' => [
                    'cancel' => [
                        'route' => back_url()
                    ],
                    'submit' => 'true'
                ]
            ])
    @else
        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'cancel' => [
                    'route' => back_url()
                ],
                    'submit' => 'true'
                ],
                'checkboxes' => [
                    'create_another' => isset($role) ? null : 'true'
                ]
            ])
    @endif


    {!! Form::close() !!}

    @include('partials.containers.portlet', [
'part' => 'close'
])
@stop


