@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
            'part' => 'open',
            'title' => 'Tracks Manager ',
            'actions' => [
                [
                    'icon' => 'fa fa-plus',
                    'title' => 'Add New',
                    'target' => route('bo.advertisers.create'),
                ]
            ]
        ])

        {!! Form::open([
            'route' => ['reports.advertisers.tracks-manager.upload'],
            'method' => 'POST',
            'files' => true,
            'class' => 'form-horizontal',
            'role' => 'form',
            'enctype' => 'multipart/form-data',
        ]) !!}


        <div class="row">
            @include('partials.fields.select', [
                'name' => 'action',
                'label' => 'Action:',
                'required' => true,
                'list' => [
                    ['id' => "add", 'text' => "Add"],
                    ['id' => "update", 'text' => "Update"],
                    ['id' => "remove", 'text' => "Remove"]
                ],
                'value' => 'add'
            ])

            @include('partials.fields.h4-separator', ['label' => 'Settings',])

            <div class="form-group form-horizontal form-md-line-input">
                @include('partials.fields.checkbox', [
                        'wrapper' => true,
                        'label' => 'Options:',
                        'list' => [
                            [
                                'name' => 'save_to_new_adserver',
                                'label' => 'Save To New Out (tracking system)',
                                'value' => 1,
                                'checked' => 1
                            ]
                        ],
                    ])
            </div>

            @include('partials.fields.input-upload', [
                'name' => 'file',
                'label' => 'File:',
                'required' => true,
            ])

        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'submit' => 'true'
            ]
        ])
        </div>
    <div class="row">
        <div class="col-md-12">
            <div class="note note-info">
                <h4 class="block">Fields</h4>
                <h5 class="block">Add</h5>
                <p>Required fields: event, token, currency, date, commission_amount, trx_id</p>
                <p>Optional fields: amount, base_commission_amount, device, io_id</p>

                <hr>
                <h5 class="block">Update</h5>
                <p>Required fields: token</p>
                <p>Optional fields: currency, date, amount, commission_amount, base_commission_amount, page, sid, network, device</p>

                <hr>
                <h5 class="block">Remove</h5>
                <p>Required fields: token</p>
            </div>
        </div>
    </div>

        <hr>


    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop