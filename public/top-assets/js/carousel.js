define('carousel', ['jquery', 'slick'], function ($) {
    var selector = '.slider-wrapper',
        default_settings = {
        arrows: true,
        centerMode: false,
        focusOnSelect: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 500,
        autoplay: true,
        useTransform: true,
        adaptiveHeight: true,
        vertical: false,
        swipe: true,
        verticalSwiping: false
    };

    function toCamel(str) {
        return str.trim().replace(/(\-|_|\s)+(.)?/g, function(mathc, sep, c) {
            return (c ? c.toUpperCase() : '');
        });
    }


    function registerArrows($slider_wrapper, carousel) {
        $carouselWrapper = $slider_wrapper.parents('.carousel-wrapper');

        $.each({
            '.previous-slide': 'slickPrev',
            '.next-slide': 'slickNext'
        }, function(selector, command ) {

            $carouselWrapper.find(selector).on('click', function() {
                carousel.slick(command);
            });
        });
    }

    function buildSettings($slider_wrapper) {
        var settings = $slider_wrapper.data();

        if (settings) {
            $.each(settings, function (key, value) {
                settings[toCamel(key)] = value;
            })
        }

        settings = $.extend({}, default_settings, settings);

        return settings;
    }

    function runSliders() {
        $(selector).each(function () {
            var $slider_wrapper = $(this);
            var settings = buildSettings($slider_wrapper);
            var carousel = $(this).slick(settings);
            registerArrows($slider_wrapper, carousel);
        });
    }

    runSliders();
});