/**
 *  Main module for dynamic dashboard
 */
define('dynamicDashboard',
  [
    'jqueryWithCustomValidation',
    'flagStats',
    'generalHelpers',
    'treeBuilder',
    'dynamicAlerts',
    'userStatus',
    'dynamicFilters',
    'dynamicTree',
    'dynamicStates'
  ]
  /**
   *  @param dynamicAlerts alerts
   */
  , function($, flag, helpers, builder, alerts, userStatus, filters, tree, states) {

    var selectors = {
      dateRangeButton: '.pickers button[type=submit]',
      status: '.note',
      resolveGroup: {
        length: '[name="resolved"]',
        type: '[data-resolve]'
      },
      resolvedAlerts: '[name="resolved"]',
      resolveAlerts: '[data-resolve]',
      prevNextButtons: '.buttons .prev, .buttons .next',
      prevNextRecords: '[data-prev] [data-next]',
      filters: {
        global: '.alert_types, .devices',
        alert_types: {
          class: '.alert_types',
          sum: '.alert_types .sum',
          length: '.alert_types .length'
        },
        devices: {
          class: '.devices',
          sum: '.devices .sum',
          length: '.devices .length'
        },
        preparationParams: {
          alert_types: {
            alert_type: {
              lowCTS: 'Low CTS',
              highCTS: 'High CTS'
            }
          },
          devices: {
            device: {
              'desktop': 'c',
              'mobile': 'm',
              'tablet': 't',
              'other': ''
            }
          }
        },
        duplicates: '.duplicates .toggle_checkbox',
        pickers: {
          arrows: '.picker .fa'
        }
      },
      listTree: {
        wrapper: '#treeWrapper',
        list: '#tree',
        item: '#tree .dd-item',
        filters: '.filters select',
        type: '#treeWrapper .listType-wrapper button',
        marked: '.marked'
      },
      alertRecords: {
        wrapper: '.alert-records'
      },
      form: {
        alertStats: '.alert-state-form',
        api: '.widget-stats-form'
      },
      sortBy: {
        wrapper: '.sort-by-buttons li'
      },
      multiSelectBox: {
        wrapper: '.alert-selection',
        selectBox: '.multi'
      }
    };

    /**
     *  @description Initalize Module
     */
    function init() {
      var form = $(selectors.form.api);
      resolveRecords(form);
      initEvents();
    }

    /**
     *  @description Update view according to selection
     */
    function updateView() {
      toggleActive(this, '.btn-group', 'button');
      updateTreeWithFilters();
    }

    /**
     *  @description Resolve Filters then refresh tree
     */
    function updateTreeWithFilters() {
      tree.resolveFilters();
      tree.init();
      tree.append();
    }

    /**
     *
     * Resolve Alert list according to duplicate states
     *
     * @param event
     */
    function resolveAlertsByDuplicateStates(event) {
      var checked = this.getAttribute('data-toggle_state') == 'checked';
      var noDuplicates = 0;
      if (checked) {
        this.setAttribute('data-toggle_state', 'notchecked');
      } else {
        this.setAttribute('data-toggle_state', 'checked');
        noDuplicates = 1;
      }

      $(selectors.form.api).find('[name=remove_duplicates]').val(noDuplicates);
      updateByPickers(event);
    }

    /**
     * @description Initalize all events
     *
     */
    function initEvents() {

      //Update record according to filters On Click
      var filterTabs = $(selectors.filters.global);
      filterTabs.on('click', updateFilters);

      //Update stats results on Date Selection
      var dateRangeButton = $(selectors.dateRangeButton);
      dateRangeButton.on('click', updateByPickers);

      //Save alert state
      var ignoreNFixButtons = $(selectors.resolveGroup.type);
      ignoreNFixButtons.on('click', saveAlertStates);

      //Prev next functionality
      var prevNextButtons = $(selectors.prevNextButtons);
      prevNextButtons.on('click', prevNextFunctionality);

      //Update view by click
      var viewTypeWrapper = $(selectors.listTree.type);
      viewTypeWrapper.on('click', updateView);

      //Update sort by click
      var sortByButtons = $(selectors.sortBy.wrapper);
      sortByButtons.on('click', resolveSortBy);

      //Add to marked on click
      var starButtons = $(selectors.listTree.wrapper);
      starButtons.on('click', selectors.listTree.marked, resolveMarkedAlerts);

      //Filter duplicates
      var duplicateButton = $(selectors.filters.duplicates);
      duplicateButton.on('click', resolveAlertsByDuplicateStates);

      //Levels arrows
      var levelArrows = $(selectors.filters.pickers.arrows);
      levelArrows.on('click', toggleArrows);

      /*Update States*/
      var treeWrapper = $(selectors.listTree.wrapper);
      treeWrapper.on('click', '.alert-item', states.init);
    }

    /**
     * @description toggle Arrows
     */
    function toggleArrows() {
      $(this).toggleClass('fa-angle-down').toggleClass('fa-angle-up');
    }

    /**
     *
     * @description Resolve sort by button
     *
     */
    function resolveSortBy() {
      toggleActive(this, '.dropdown-menu', 'li');
      updateTreeWithFilters();
    }

    /**
     *
     * @description Resolve marked alerts
     *
     */
    function resolveMarkedAlerts() {
      tree.toggleMarked(this);
      tree.saveMarkedAlerts(this);
    }

    /**
     * @description Assign active class to viewButton type
     *
     * @param item
     */
    function toggleActive(item, wrapper, elem) {
      var $item = $(item);
      var buttons = $item.closest(wrapper).find(elem);
      buttons.removeClass('active');
      $item.addClass('active');
    }

    /**
     *  @description Save Alert States After Click
     *
     * @param e
     */
    function saveAlertStates(e) {
      e.preventDefault();

      states.save(e)
        .then(function() {
          var form = $(selectors.form.api);
          updateResolved();
          resolveRecords(form);

          /**
           *  @description update resolved alert in the status bar
           */
          function updateResolved() {
            var resolved = parseInt($(selectors.resolveGroup.length).val());
            resolved = resolved + 1;
            $(selectors.resolveGroup.length).val(resolved);
          }
        })
        .catch(function(error) {
          console.log(error);
        });
    }

    /**
     *
     *  @description Prev next functionality
     *
     * @param e
     *
     */
    function prevNextFunctionality(e) {
      e.preventDefault();
      var btn = $(e.target);

      if (btn.data('type') === 'next') {
        tree.nextItem();
      } else if (btn.data('type') === 'prev') {
        tree.prevItem();
      }
    }

    /**
     *
     * @description Update by filter pickers
     *
     * @param e
     */
    function updateByPickers(e) {
      e.preventDefault();

      var $form = $(selectors.form.api);
      $form.find('input[name="filtered"]').val('true');
      resolveOperator($form);
      resolveRecords($form);

      /**
       *
       * @param $form
       */
      function resolveOperator($form) {
        var operator;

        $form.find('.filter-field').each(function(key, filter) {
          var amount = $(filter).find('[type=number]').filter(function(key, amount) {

            return amount.value != '';
          });

          if (amount.length) {
            operator = amount[0].getAttribute('data-operator');

            if (amount.length > 1) {
              operator = '><';
            }

            $(filter).find('.operator').val(operator);
          }
        });
      }
    }

    /**
     * @description - Resolve records from Backend
     *
     * @param form
     */
    function resolveRecords($form) {
      var promise = $.Deferred();

      //process data after getting success result
      this.prepare = function(records) {
        filters.records = records.data;
        var filteredState = $form.find('input[name="filtered"]').val();

        //if filter is set (update info bar)
        if (filteredState) {
          $(selectors.resolveGroup.length).val(records.resolved);
        }
        this.append(filters.init());
      };

      //make ajax call
      this.ajax = function() {
        var params = $form.serializeArray();
        var that = this;
        var setting = {
          method: $form.attr('method'),
          url: $form.attr('action'),
          data: params,
          beforeSend: function() {
            App.blockUI({iconOnly: true, target: $form.find('.widget-thumb')});
          },
          complete: function(response) {

            if (helpers.isJson(response.responseText)) {
              var records = response.responseJSON;
              if (records.error) {
                App.unblockUI($form.find('.widget-thumb'));

                toastr.warning(records.error);
                return;
              }
            } else {
              toastr.warning('there was some problem with the data ');
              App.unblockUI($form.find('.widget-thumb'));

              return console.warn('there was some problem with the data ');
            }

            that.prepare(records);
          }
        };

        $.ajax(setting);
      };

      //Append data to dom
      this.append = function(filteredRecords) {
        injectUserStatus(filteredRecords);
        injectTree();
        App.unblockUI($form.find('.widget-thumb'));

        /**
         *
         * @param filteredRecords
         */
        function injectUserStatus(filteredRecords) {
          var resolved = parseInt($(selectors.resolveGroup.length).val());
          var ratio = ((resolved / filteredRecords.length) * 100).toFixed(2);
          var options = {
            'length': filteredRecords.length,
            'fixed': resolved,
            'percentage': ratio
          };

          $.each(options, function(subject, record) {
            $(selectors.status + ' .' + subject).html(record);
          });
        }

        function injectTree() {
          resolveTree();
          promise.resolve();
        }
      };

      this.ajax();

      return promise;
    }

    /**
     @description -  Update results  on selection

     - find the filter
     - activate it
     - find other filtered that already activated
     - filter them
     - update dom results

     **/
    function updateFilters(event) {
      event.preventDefault();
      var thumb = $(event.target).closest('.widget-thumb');
      resolveTree(thumb);
    }

    /**
     *
     * Resolve tree process
     *
     * @param thumb
     */
    function resolveTree(thumb) {
      filters.init(thumb);
      tree.records = filters.filtered;
      tree.init();
      tree.append();
    }

    init();
  });
