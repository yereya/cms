<?php

use App\Entities\Models\Sites\Like;
use App\Entities\Models\Sites\Post;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AuthRouteTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * testRoutesNotFailing
     */
    public function testArtisanRouteListNotFailing()
    {
        Artisan::call('route:list');

        // If not exception thrown assert true
        $this->assertTrue(true);
    }


    /**
     * Test the homepage works and the dashboard button appears.
     */
    public function testAdminHomePageLoggedIn()
    {
        $this->setUsers();
        $this->actingAs($this->super_admin_user)
            ->visit('/')
            ->see('Daily Stats')
            ->see('Roles')
            ->see($this->super_admin_user->name);
    }

    public function setUp()
    {
        parent::setUp();

    }

    /**
     * Test the logout button redirects the user back to home and the login button is again visible.
     */
    public function testLogoutRoute()
    {
        // Make sure our events are fired
        $this->setUsers();
        $this->actingAs($this->super_admin_user)
            ->visit('/')
            ->visit('/auth/logout')
            ->see('Login to your account');
    }

    /**
     * Test the generic 404 page.
     */
//    public function test404Page()
//    {
//        $response = $this->call('GET', '7g48hwbfw9eufj');
//        $this->assertEquals(404, $response->getStatusCode());
//        $this->assertContains('RouteCollection.php line 161:', $response->getContent());
//    }

}
